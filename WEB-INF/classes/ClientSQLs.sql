-- ALTER TABLE ACCESSCONTROL.users ADD company VARCHAR(45) AFTER loginname;
-- ALTER TABLE ACCESSCONTROL.users ADD country VARCHAR(45) AFTER company;
-- ALTER TABLE ACCESSCONTROL.users ADD isActivated boolean AFTER password;
-- ALTER TABLE ACCESSCONTROL.users ADD activationKey VARCHAR(45) AFTER isActivated;
-- ALTER TABLE ACCESSCONTROL.users ADD passwordResetKey VARCHAR(45) AFTER activationKey;
-- ALTER TABLE ACCESSCONTROL.users ADD photo longblob AFTER email_id;
-- ALTER TABLE ACCESSCONTROL.users ADD jobTitle VARCHAR(100) AFTER company;
-- ALTER TABLE ACCESSCONTROL.users ADD city VARCHAR(100) AFTER country;


drop table if exists REVINFO;
create table REVINFO (
REV integer unsigned NOT NULL AUTO_INCREMENT,
REVTSTMP bigint,
primary key (REV)
);

drop table if exists scheduled_downtime;
create table scheduled_downtime(
id int(10) unsigned NOT NULL AUTO_INCREMENT, 
location_name varchar(100) NOT NULL,	
bandwidth varchar(100) NOT NULL,
application_name varchar(100) NOT NULL,	
page_name varchar(100) NOT NULL,
start_time datetime NOT NULL,
end_time datetime NOT NULL,
scheduled_by  varchar(100) NOT NULL,
last_update_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (id),
UNIQUE KEY unique_key_scheduled_downtime (location_name, bandwidth, application_name, page_name)
);

drop table if exists scheduled_downtime_AUD;
create table scheduled_downtime_AUD(
REV integer not null,
REVTYPE tinyint,
id int(10) unsigned NOT NULL, 
location_name varchar(100),	
bandwidth varchar(100),
application_name varchar(100),	
page_name varchar(100),
start_time datetime,
end_time datetime,
scheduled_by varchar(100),
last_update_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
primary key (id, REV)
) ENGINE=INNODB;


insert into scheduled_downtime values(1, 'Mumbai-QK', '524288', 'AxisApp', 'MBLoginPage', '2016-06-24 00:00:00','2016-06-24 02:00:00','Shubhangi',NOW());
insert into scheduled_downtime values(2, 'Mumbai-QK', '524288', 'AxisApp', 'MBAfterLoginPage', '2016-06-15 13:00:00','2016-06-13 15:30:00','Shubhangi',NOW());
insert into scheduled_downtime values(3, 'Mumbai-QK', '524288', 'AxisApp', 'MobileBankingPage', '2016-06-09 18:00:00','2016-06-09 18:30:00','Shubhangi',NOW());

drop table if exists alerts_AUD;
create table alerts_AUD(
REV integer not null,
REVTYPE tinyint,
id int(10) unsigned NOT NULL,
error_time datetime, 
location_name varchar(100),	
bandwidth varchar(100),
application_name varchar(100), 
transaction_name varchar(100),
page_name varchar(100), 
description varchar(100),
state varchar(100), 
stop_escalation boolean,
stop_escalation_start_time datetime,
stop_escalation_end_time datetime,
stop_escalation_reason varchar(100),
last_modified_by varchar(100),
last_update_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
primary key (id, REV)
) ENGINE=INNODB;

drop table if exists alerts;
create table alerts(
id int(10) unsigned NOT NULL AUTO_INCREMENT,
error_time datetime NOT NULL, 
location_name varchar(100) NOT NULL,	
bandwidth varchar(100) NOT NULL,
application_name varchar(100) NOT NULL, 
transaction_name varchar(100) NOT NULL,
page_name varchar(100) NOT NULL, 
description varchar(100) NOT NULL,
state varchar(100) NOT NULL, 
stop_escalation boolean NOT NULL DEFAULT FALSE,
stop_escalation_start_time datetime,
stop_escalation_end_time datetime,
stop_escalation_reason varchar(100),
last_modified_by varchar(100),
last_update_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (id),
UNIQUE KEY unique_key_alerts (location_name, bandwidth, application_name, transaction_name, page_name, error_time)
) ENGINE=INNODB;

insert into alerts values(1, '2016-06-09 18:40:50', 'Mumbai-QK', '524288', 'AxisApp', 'Mobile_Login', 'MBLoginPage', 'test alert',      'Active', false, null, null, null, 'Shubhangi', NOW());
insert into alerts values(2, '2016-06-14 20:20:23', 'Mumbai-QK', '524288', 'AxisApp', 'Mobile_Login', 'MBAfterLoginPage', 'test alert', 'Stop escalation', false, null, null, null, 'Shubhangi', NOW());
insert into alerts values(3, '2016-06-16 03:40:01', 'Mumbai-QK', '524288', 'AxisApp', 'Mobile_Login', 'MBLoginPage', 'test alert',      'Working fine', false, null, null, null, 'Shubhangi',  NOW());


drop table if exists client_requested_applications;
create table client_requested_applications(
id int(10) unsigned NOT NULL AUTO_INCREMENT,
job_title varchar(100) NOT NULL, 
mobile_number bigint(20) unsigned NOT NULL,	
client_name varchar(100) NOT NULL,
application_name varchar(100) NOT NULL, 
location_name varchar(100) NOT NULL,
cron_expression varchar(50) NOT NULL, 
protocol varchar(10) NOT NULL,
url varchar(200) NOT NULL, 
application_type varchar(100) NOT NULL,
application_sub_type varchar(100),
platform varchar(100),
browser varchar(100),
last_update_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (id)
) ENGINE=INNODB;

drop table if exists escalation_matrix;
create table escalation_matrix(
id int(10) unsigned NOT NULL AUTO_INCREMENT,
client_name varchar(100) NOT NULL, 
application_name varchar(100) NOT NULL, 
level varchar(100) NOT NULL,
user_type varchar(100) NOT NULL, 
user_name varchar(100) NOT NULL,
job_title varchar(100) NOT NULL, 
email_id varchar(100) NOT NULL,
mobile_number bigint(20) unsigned NOT NULL,	
last_update_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (id)
) ENGINE=INNODB;

drop table if exists application_screenshot_upload;
create table application_screenshot_upload(
id int(10) unsigned NOT NULL AUTO_INCREMENT,
client_name varchar(100) NOT NULL, 
application_name varchar(100) NOT NULL, 
transaction_name varchar(100) NOT NULL,
page_name varchar(100) NOT NULL, 
application_type varchar(100),
application_sub_type varchar(100),
platform varchar(100),
browser varchar(100),
description varchar(100) NOT NULL,
image_name varchar(100) NOT NULL, 
image longblob NOT NULL,
last_update_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (id)
) ENGINE=INNODB;


