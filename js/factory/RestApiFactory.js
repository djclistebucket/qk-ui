'use strict';

app.factory('RestApiFactory', ['$http', '$q', '$rootScope', function ($http, $q,$rootScope) {

  var RestApiFactory = {};

  /* Retrieving User's application */
  RestApiFactory.getApplication = function (appParam) {


    var locationName = appParam.locationName;
    var bandwidth = appParam.bandwidth;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var apiurl =  appParam.urlApp;


    console.log("locationName===" + locationName);
    console.log("bandwidth===" + bandwidth);
    console.log("emailId===" + emailId);
    console.log("password===" + password);
    console.log("apiurl===" + apiurl);

    var deferred = $q.defer();

    var promise1 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      return response.data;
    }, function (response) {
      console.log("response" + response);
      return response.data;
    });
    return promise1;
  };

  /* Retrieving Chart data */

  RestApiFactory.getChartData = function (appParam) {

    var locationName = appParam.locationName;
    var bandwidth = appParam.bandwidth;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var apiurl =  appParam.urlApp;
    var startTime = appParam.startTime;
    var endTime = appParam.endTime;


    console.log("locationName===" + locationName);
    console.log("bandwidth===" + bandwidth);
    console.log("emailId===" + emailId);
    console.log("password===" + password);
    console.log("apiurl===" + apiurl);
    console.log("startTime===" + startTime);
    console.log("endTime===" + endTime);

    var deferred = $q.defer();
    var promise2 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        startTime: startTime,
        endTime: endTime
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }
      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise2;
  };

  /* User Registration Rest API Call  */

  RestApiFactory.getUserRegisteration = function (appParam) {

    var emailId = appParam.emailId;
    var name = appParam.name;
    var companyName = appParam.companyName;
    var country = appParam.country;
    var password = appParam.password;
    var apiurl =  appParam.urlApp;


    console.log("emailId===" + emailId);
    console.log("name===" + name);
    console.log("companyName===" + companyName);
    console.log("country===" + country);
    console.log("password===" + password);
    console.log("apiurl===" + apiurl);


      var deferred = $q.defer();
      var promise3 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        emailId: emailId,
        name: name,
        company: companyName,
        country: country,
        password: password,
        confirmPassword: password

      }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }
      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise3;

  };

  /* User Activation Rest API Call  */
  RestApiFactory.getUserActivation = function (appParam) {
    var apiurl =  appParam.urlApp;
    console.log("apiurl===" + apiurl);
    var deferred = $q.defer();
    var promise4 = $http({
      method: 'GET',
      url: apiurl,
      headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }
      return deferred.promise;
    }, function (response) {
      //error
      console.log("response" + response);
      return response.data;

    });
    return promise4;

  };

  /* User Authentication Rest API Call  */

  RestApiFactory.getUserAuthentication = function (appParam) {

    var apiurl =  appParam.urlApp;
    var emailId = appParam.emailId;
    var password = appParam.password;

    console.log("emailId===" + emailId);
    console.log("password===" + password);
    console.log("apiurl===" + apiurl);


    var deferred = $q.defer();
    var promise5 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        emailId: emailId,
        password: password

      }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }
      return deferred.promise;
    }, function (response) {
      //error
      console.log("response" + response);
      return response.data;

    });
    return promise5;

  };


  /* Drilldown Menu Rest API Call  */

  RestApiFactory.getDrilldownMenu = function (appParam) {

    var apiurl =  appParam.urlApp;
    console.log("apiurl===" + apiurl);


    var deferred = $q.defer();
    var promise6 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({}),
      headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }
      return deferred.promise;
    }, function (response) {
      //error
      console.log("response" + response);
      return response.data;

    });
    return promise6;

  };


  /* Retrieving Chart data */

  RestApiFactory.getApplicationDrillData = function (appParam) {

    var locationName = appParam.locationName;
    var bandwidth = appParam.bandwidth;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var apiurl =  appParam.urlApp;
    var startTime = appParam.startTime;
    var endTime = appParam.endTime;
    var transactionNames = appParam.transactionNames;


    console.log("locationName===" + locationName);
    console.log("bandwidth===" + bandwidth);
    console.log("emailId===" + emailId);
    console.log("password===" + password);
    console.log("apiurl===" + apiurl);
    console.log("startTime===" + startTime);
    console.log("endTime===" + endTime);
    console.log("transactionNames===" + transactionNames);
    var jsonData = angular.toJson(transactionNames);
    console.log("transactionNames===" + transactionNames.toString());
    console.log("jsonData===" + jsonData);

    var deferred = $q.defer();
    var promise7 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        startTime: startTime,
        endTime: endTime,
        transactionNames: transactionNames.toString()
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }
      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      if (response.status === 500) {
        console.log('server error');
        var response = {"message": "No data available.", "statusCode": 1};
        return response
      }
      return response.data;

    });
    return promise7;
  };

  RestApiFactory.getPageDrillData = function (appParam) {

    var locationName = appParam.locationName;
    var bandwidth = appParam.bandwidth;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var apiurl =  appParam.urlApp;
    var startTime = appParam.startTime;
    var endTime = appParam.endTime;
    var pageNames = appParam.pageNames;

    console.log("locationName===" + locationName);
    console.log("bandwidth===" + bandwidth);
    console.log("emailId===" + emailId);
    console.log("password===" + password);
    console.log("apiurl===" + apiurl);
    console.log("startTime===" + startTime);
    console.log("endTime===" + endTime);
    console.log("pageNames===" + pageNames);


    var deferred = $q.defer();
    var promise8 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        startTime: startTime,
        endTime: endTime,
        pageNames: pageNames.toString()
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }
      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise8;
  };


  RestApiFactory.getErrorData = function (appParam) {

    var locationName = appParam.locationName;
    var bandwidth = appParam.bandwidth;
    var apiurl =  appParam.urlApp;
    var startTime = appParam.startTime;
    var endTime = appParam.endTime;
    var emailId = appParam.emailId;
    var password = appParam.password;


    console.log("locationName===" + locationName);
    console.log("bandwidth===" + bandwidth);
    console.log("apiurl===" + apiurl);
    console.log("startTime===" + startTime);
    console.log("endTime===" + endTime);
    console.log("emailId===" + emailId);
    console.log("password===" + password);


    var deferred = $q.defer();
    var promise9 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        startTime: startTime,
        endTime: endTime
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }
      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise9;
  };


  RestApiFactory.getTransactionErrorData = function (appParam) {

    var locationName = appParam.locationName;
    var bandwidth = appParam.bandwidth;
    var apiurl =  appParam.urlApp;
    var startTime = appParam.startTime;
    var endTime = appParam.endTime;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var pageNames = appParam.pageNames;


    console.log("locationName===" + locationName);
    console.log("bandwidth===" + bandwidth);
    console.log("apiurl===" + apiurl);
    console.log("startTime===" + startTime);
    console.log("endTime===" + endTime);
    console.log("emailId===" + emailId);
    console.log("password===" + password);
    console.log("pageNames===" + pageNames.toString());


    var deferred = $q.defer();
    var promise10 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        startTime: startTime,
        endTime: endTime,
        pageNames: pageNames.toString()
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }
      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise10;
  };

  /* get Active Alerts  Rest API Call  */

  RestApiFactory.getActiveAlerts = function (appParam) {
   
    var apiurl = appParam.urlApp;
    var emailId = appParam.emailId;
    var password = appParam.password;

    var deferred = $q.defer();
    var promise11 = $http({
      method: 'GET',
      url: apiurl,
      data: $.param({
        emailId: emailId,
        password: password

      }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }

      
      return deferred.promise;
    }, function (response) {
      //error
      console.log("response" + response);
      return response.data;

    });

    return promise11;
  };

  /* my action stop escalation */

  RestApiFactory.myActionStopEscalation = function (appParam) {
   
    var locationName = appParam.locationName;
    var applicationName = appParam.applicationName;
    var bandwidth = appParam.bandwidth;
    var apiurl =  appParam.urlApp;
    var errorTime = appParam.errorTime;
    var transactionName = appParam.transactionName;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var pageName = appParam.pageName;
    var stopEscalationStartTime = appParam.stopEscalationStartTime;
    var stopEscalationEndTime = appParam.stopEscalationEndTime;
    var stopEscalationReason = appParam.stopEscalationReason;
    var user = appParam.user;


    var deferred = $q.defer();
    var promise12 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        stopEscalationStartTime: stopEscalationStartTime,
        stopEscalationEndTime: stopEscalationEndTime,
        errorTime: errorTime,
        transactionName: transactionName,
        applicationName: applicationName,
        pageName: pageName,
        user: user,
        stopEscalationReason: stopEscalationReason
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }

      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise12;
  };

  /* my action cancel stop escalation */

  RestApiFactory.cancelStopEscalation = function (appParam) {
    
    var locationName = appParam.locationName;
    var applicationName = appParam.applicationName;
    var bandwidth = appParam.bandwidth;
    var apiurl =  appParam.urlApp;
    var errorTime = appParam.errorTime;
    var transactionName = appParam.transactionName;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var pageName = appParam.pageName;
    var user = appParam.user;


    var deferred = $q.defer();
    var promise13 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        errorTime: errorTime,
        transactionName: transactionName,
        applicationName: applicationName,
        pageName: pageName,
        user: user
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }

      
      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise13;
  };

//schedule downtime data

  RestApiFactory.getScheduleDowntimeAll = function (appParam) {
   
    var apiurl = appParam.urlApp;
    var emailId = appParam.emailId;
    var password = appParam.password;

    var deferred = $q.defer();

    var promise14 = $http({
      method: 'GET',
      url: apiurl,
      data: $.param({
        emailId: emailId,
        password: password

      }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }

      return deferred.promise;
    }, function (response) {
      //error
      console.log("response" + response);
      return response.data;

    });

    return promise14;
  };

// save schedule downtime

  RestApiFactory.saveNewDowntime = function (appParam) {
   
    var locationName = appParam.locationName;
    var applicationName = appParam.applicationName;
    var bandwidth = appParam.bandwidth;
    var apiurl =  appParam.urlApp;
    var startTime = appParam.startTime;
    var endTime = appParam.endTime;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var scheduledBy = appParam.scheduledBy;


    var deferred = $q.defer();
    var promise15 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        startTime: startTime,
        endTime: endTime,
        applicationName: applicationName,
        scheduledBy: scheduledBy
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }

      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise15;
  };

//delete schedule downtime

  RestApiFactory.DeleteDowntime = function (appParam) {
   
    var locationName = appParam.locationName;
    var applicationName = appParam.applicationName;
    var bandwidth = appParam.bandwidth;
    var apiurl =  appParam.urlApp;
    var emailId = appParam.emailId;
    var password = appParam.password;


    var deferred = $q.defer();
    var promise16 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        applicationName: applicationName
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }

      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise16;
  };

// update schedule downtime by application

  RestApiFactory.UpdateDowntimebyApp = function (appParam) {

    var locationName = appParam.locationName;
    var applicationName = appParam.applicationName;
    var pageNames = appParam.pageNames;
    var bandwidth = appParam.bandwidth;
    var apiurl =  appParam.urlApp;
    var startTime = appParam.startTime;
    var endTime = appParam.endTime;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var scheduledBy = appParam.scheduledBy;


    var deferred = $q.defer();
    var promise17 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        startTime: startTime,
        pageNames:pageNames,
        endTime: endTime,
        applicationName: applicationName,
        scheduledBy: scheduledBy
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }

      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise17;
  };


  // save schedule downtime by pages

  RestApiFactory.saveDowntimebyPages = function (appParam) {

    var locationName = appParam.locationName;
    var applicationName = appParam.applicationName;
    var pageNames = appParam.pageNames;
    var bandwidth = appParam.bandwidth;
    var apiurl =  appParam.urlApp;
    var startTime = appParam.startTime;
    var endTime = appParam.endTime;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var scheduledBy = appParam.scheduledBy;

    var deferred = $q.defer();
    var promise18 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        pageNames:pageNames,
        startTime: startTime,
        endTime: endTime,
        applicationName: applicationName,
        scheduledBy: scheduledBy
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }

      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise18;
  };

//delete schedule downtime by pages

  RestApiFactory.DeleteDowntimebyPages = function (appParam) {

    var locationName = appParam.locationName;
    var applicationName = appParam.applicationName;
    var pageNames = appParam.pageNames;
    var bandwidth = appParam.bandwidth;
    var apiurl =  appParam.urlApp;
    var emailId = appParam.emailId;
    var password = appParam.password;

    var deferred = $q.defer();
    var promise19 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        pageNames:pageNames,
        applicationName: applicationName
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }

      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise19;
  };

// update schedule downtime by pages

  RestApiFactory.UpdateDowntimebyPages = function (appParam) {

    var locationName = appParam.locationName;
    var applicationName = appParam.applicationName;
    var pageNames = appParam.pageNames;
    var bandwidth = appParam.bandwidth;
    var apiurl =  appParam.urlApp;
    var startTime = appParam.startTime;
    var endTime = appParam.endTime;
    var emailId = appParam.emailId;
    var password = appParam.password;
    var scheduledBy = appParam.scheduledBy;


    var deferred = $q.defer();
    var promise20 = $http({
      method: 'POST',
      url: apiurl,
      data: $.param({
        locationName: locationName,
        bandwidth: bandwidth,
        startTime: startTime,
        pageNames:pageNames,
        endTime: endTime,
        applicationName: applicationName,
        scheduledBy: scheduledBy
      }),
      headers: {'x': emailId, 'y': password, 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

    }).then(function (response) {

      if (typeof response.data === 'object') {

        deferred.resolve(response.data);
      } else {
        return $q.reject(response.data);
      }

      return deferred.promise;
    }, function (response) {
      console.log("response" + response);
      return response.data;

    });
    return promise20;
  };

  return RestApiFactory;
}
]);

/* DateUtil Service  */

app.factory('DateUtilService', function () {
  var factory = {};

  factory.startDate = function (a) {
    console.log("hours==" + a);
    var startTime = (new Date(new Date().getTime() - (a * 60 * 60 * 1000))).getTime();
    console.log("startTime==" + startTime);
    return startTime;
  };

  factory.endDate = function () {

    var endDate = new Date().getTime();
    console.log("endDate==" + endDate);
    return endDate;
  };

  return factory;
});


