

/**
 * Main AngularJS Web Application
 * 
 * 
 */

var app = angular.module('cxGuardianWebApp', ['ngRoute','UserValidation','highcharts-ng','ui.bootstrap','angularjs-datetime-picker']);


//This service reads data from the query string into a filter object.
app.service('QueryStringService', function ($location) {
    this.getFilters = function(filterObj) {
        var qs = $location.search();
        for (var param in filterObj) {
            if (param in qs) {
            	console.log("param=="+param);
                filterObj[param] = qs[param];
            }
        }
        return filterObj;
    };
});



angular.module('UserValidation', []).directive('validPasswordC', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var noMatch = viewValue != scope.userRegForm.password.$viewValue
                ctrl.$setValidity('noMatch', !noMatch)
            })
        }
    }
});


app.filter('time', function($filter)
		{
		 return function(input)
		 {
		  if(input == null){ return ""; } 
		 
		  var _date = $filter('date')(new Date(input), 'HH:mm');
		 
		  return _date.toUpperCase();

		 };
		});


app.filter('day', function($filter)
		{
		 return function(input)
		 {
		  if(input == null){ return ""; } 
		 
		  var _date = $filter('date')(new Date(input), 'dd');
		 
		  return _date.toUpperCase();

		 };
		});

app.filter('mmyy', function($filter)
		{
		 return function(input)
		 {
		  if(input == null){ return ""; } 
		 
		  var _date = $filter('date')(new Date(input), 'MMM yyyy');
		 
		  return _date.toUpperCase();

		 };
		});

	
	

app.run(['$rootScope', '$location', function ($rootScope, $location) {

  $rootScope.loader = false;
    $rootScope.$on('$routeChangeStart', function (event) {
     $rootScope.location = $location;
     
     var loggedIn = JSON.parse(localStorage.getItem('loggedIn'));
     console.log("loggedIn=="+loggedIn);
     
       if(loggedIn) {
         
    	 $rootScope.loggedIn = loggedIn;
         console.log("$rootScope.loggedIn=="+$rootScope.loggedIn);
         
    	// var clientName = JSON.parse(localStorage.getItem('clientName'));
        // console.log("clientName=="+clientName);
       //  $rootScope.clientName = clientName;
         
        
     }
     
    
});
   
}]);



var myTemplate = '<div data-text="{{$scope.value}}"></div>';
var percentage = {};

app.directive('circful', function() {
   return {
     scope:{
        value : '='
     },
     restrict: 'E',
     template: myTemplate,
     link : function(scope,element){
       percentage.animationStep = 5;
       percentage.foregroundColor = '#ff6600';
       percentage.backgroundColor = '#eceaeb';
       percentage.fontColor = '#2A3440';
       percentage.foregroundBorderWidth = 10;
       percentage.backgroundBorderWidth = 10;
       percentage.pointSize = 100;
       percentage.percent = scope.value;
       percentage.percentages = [10, 20, 30];
       percentage.multiPercentage= 1;
       element.circliful(percentage);
     }
   };
 });



app.directive('simpleCaptcha', function() {
    return {
        restrict: 'E',
        scope: { valid: '=' },
        template: '<input ng-model="a.value" ng-show="a.input" style="width:2em; text-align: center;"><span ng-hide="a.input">{{a.value}}</span>&nbsp;{{operation}}&nbsp;<input ng-model="b.value" ng-show="b.input" style="width:2em; text-align: center;"><span ng-hide="b.input">{{b.value}}</span>&nbsp;=&nbsp;{{result}}',
        controller: function($scope) {
            
            var show = Math.random() > 0.5;
            
            var value = function(max){
                return Math.floor(max * Math.random());
            };
            
            var int = function(str){
                return parseInt(str, 10);
            };
            
            $scope.a = {
                value: show? undefined : 1 + value(4),
                input: show
            };
            $scope.b = {
                value: !show? undefined : 1 + value(4),
                input: !show
            };
            $scope.operation = '+';
            
            $scope.result = 5 + value(5);
            
            var a = $scope.a;
            var b = $scope.b;
            var result = $scope.result;
            
            var checkValidity = function(){
                if (a.value && b.value) {
                    var calc = int(a.value) + int(b.value);
                    $scope.valid = calc == result;
                } else {
                    $scope.valid = false;
                }
                $scope.$apply(); // needed to solve 2 cycle delay problem;
            };
            
            
            $scope.$watch('a.value', function(){    
                checkValidity();
            });
            
            $scope.$watch('b.value', function(){    
                checkValidity();
            });
            
            
            
        }
    };
});





app.factory('AppsService', function ($http, $q) {
    return {
        getApplication: function(appParam) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
        	
        	 var locationName = appParam.locationName;
        	 var bandwidth =  appParam.bandwidth;
        	 var emailId = appParam.emailId;
        	 var password = appParam.password;
        	 var  apiurl = appParam.urlApp;
        	 
        	console.log("locationName==="+locationName);
        	console.log("bandwidth==="+bandwidth);
        	console.log("emailId==="+emailId);
        	console.log("password==="+password);
        	console.log("apiurl==="+apiurl);
        	
        	var deferred = $q.defer();
        	 
          var promise1 = $http({
        	  method: 'POST',
    	        url: apiurl,
    	        data: $.param({
    	        	locationName:  locationName,
    	        	bandwidth: bandwidth
    	        }),
    	        headers: 
    	      {'x':emailId, 'y':password,'Content-Type':'application/x-www-form-urlencoded;charset=utf-8'}
      	 
          }).then(function(response) {
        	  if (typeof response.data === 'object') {
        		  deferred.resolve(response.data);
                  return deferred.promise;
              } else {
                  // invalid response
                  return $q.reject(response.data);
              }
          }, function(response) {
              // something went wrong
              return $q.reject(response.data);
          });
          
          
          return promise1;
        }
    };
    
    
});



/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider,$rootScope) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "partials/dashboard.html",controller: "DashboardCtrl",
       resolve :  {
    	"check" : function($location,$rootScope){
    		
    		console.log("$rootScope.loggedIn=="+$rootScope.loggedIn);
    		if(!$rootScope.loggedIn){
    			$location.path("/login");
    		}else{
    		 
    			$location.path("/dashboard");
    		}
    	} 
       
       }	
    
    })
    // Pages
    .when("/about", {templateUrl: "partials/about.html", controller: "PageCtrl"})
    .when("/login", {templateUrl: "partials/login.html", controller: "LoginCtrl",
    	 resolve :  {
    	    	"check" : function($location,$rootScope){
    	    		
    	    		console.log("$rootScope.loggedIn=="+$rootScope.loggedIn);
    	    		console.log("$rootScope.errorMessage=="+$rootScope.errorMessage);
    	    		if(!$rootScope.loggedIn){
    	    			$location.path("/login");
    	    		}else{
    	    		 
    	    			 
    	    			$location.path("/dashboard");
    	    		}
    	    	} 
    	 }
    
    })
    .when("/quote", {templateUrl: "partials/quote.html", controller: "PageCtrl"})
	.when("/dashboard", {templateUrl: "partials/dashboard.html", controller: "DashboardCtrl",
		
		 resolve :  {
 	    	"check" : function($location,$rootScope){
 	    		
 	    		console.log("$rootScope.loggedIn=="+$rootScope.loggedIn);
 	    		if(!$rootScope.loggedIn){
 	    			$location.path("/login");
 	    		}else{
 	    			 	    			  
 	    			$location.path("/dashboard");
 	    		}
 	    	} 
 	 }
	
	})
    .when("/services", {templateUrl: "partials/services.html", controller: "PageCtrl"})
    .when("/contact", {templateUrl: "partials/contact.html", controller: "PageCtrl"})
     .when("/register", {templateUrl: "partials/registration.html", controller: "RegistrationCtrl"})
     .when("/drilldown", {templateUrl: "partials/drilldown.html"})
     .when("/signup/:param1/:param2", {templateUrl: "partials/login.html", controller: "SignupCtrl"})
     .when("/verify-email", {templateUrl: "partials/verify-email.html",controller: "RegistrationCtrl"})
      .when("/logout", {templateUrl: "partials/logout.html",controller: "LogoutCtrl"})
      .when("/myaction", {templateUrl: "partials/myaction.html",controller: "MyActionCtrl"})
      
      
      .when("/addapplication", {templateUrl: "partials/add-application.html",controller: "AddApplicationCtrl"})
          
          
          
          
    // else 404
    .otherwise("/404", {templateUrl: "partials/404.html", controller: "PageCtrl"});
}]);


/**
 * Controls all other Pages
 */
app.controller('PageCtrl',  function ( $scope, $window /* $location, $http */) {
  console.log("Page Controller reporting for duty.");
  
  var myTemplate = '<div data-text="{{$scope.value}}"></div>';
  var percentage = {};

 
  
  $scope.helloTo = {};
  $scope.helloTo.title = "AngularJS";
  console.log( $scope.helloTo.title);
  $scope.message= "Your IDigital Application validity is about to expire on 18/05/2016. Kindly Contact our Transition Manager.";
  console.log($window.sessionStorage["userInfo"])
  // create a blank object to hold our form information
  // $scope will allow this to pass between controller and view
  $scope.formData = {};
    
});





