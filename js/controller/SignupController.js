'use strict';

/* SignupCtrl :- This controller is used for activating user */

app.controller('SignupCtrl', ['$scope','$routeParams','$rootScope','$window','$location', 'RestApiFactory', function($scope, $routeParams,$rootScope,$window,$location,RestApiFactory) {
	  console.log("SignupCtrl reporting for duty.");
	  
	  var param1 = $routeParams.param1;
	  var param2 = $routeParams.param2;
	  
	  console.log("param1=="+param1);
	  console.log("param2=="+param2);
	  var urlApp = "rest/registration/signup/doActivate?"+param1+"&"+param2;
	  
	  var activationdetails = {};
	 // activationdetails.emailId = param1;
	 // activationdetails.activationCode = param2;
	  activationdetails.urlApp = urlApp;
	  
	  
	  $rootScope.errorMessage ="";
	  $scope.searchButtonText = "LOG IN";
	  
	  var promise =  RestApiFactory.getUserActivation(activationdetails).then(function(data)
     		    {
     		   console.log("getUserActivation==="+ JSON.stringify(data));
     		   var statusCode = data.statusCode;
     		   
     		   if(statusCode == "1"){
     				
     			  $rootScope.errorMessage = data.message;
     			 console.log(" $roootScope.message==="+  $rootScope.message); 
     			   
     		   }else{
     			  $rootScope.errorMessage = data.message;
     			  
     		   }
     		   
     		    });
	  
	  
	  $scope.submitForm = function() {
			 
			 //This code is for spinning 
			 $scope.loading = true;
		    $scope.searchButtonText = "Loading";
			 
			
		       
			// console.log("email==="+$scope.user.email);
			// console.log("password==="+$scope.user.password);
			 var urlApp = '/rest/accesscontrol/authenticate/login';
			 var appParam = {};
			 appParam.emailId = $scope.user.email;
			 appParam.password = $scope.user.password;
			 appParam.urlApp = urlApp;
			 
			 
			 var promise =  RestApiFactory.getUserAuthentication(appParam).then(function(data)
		       		    {
		       		     
				    console.log(data);
			    	console.log(data.authenticated);
			    	var auth = data.authenticated;
			    	var clientName = data.clientName;
			    	var user = data.userName;
			    	var phoneNumber = data.phoneNumber;
			    	var emailId = data.emailId;
			    	console.log("auth = "+auth);
			    	console.log("client = "+clientName);
			    	console.log("user = "+user);
			    	console.log("emailId = "+emailId);
			    	
			    	
			    	if(auth){
			    		  $rootScope.userName = user;
			    		  $rootScope.emailId = emailId;
			    		  $rootScope.password = $scope.user.password;
			    		  $rootScope.clientName = clientName;
			    		  $rootScope.loggedIn = true;
			    		  
			    		
			    		  
			    		var userInfo = {
			    			        accessToken:auth,
			    			        userName: user,
			    			        clientName: clientName
			    			      };
			    			      $window.sessionStorage["userInfo"] = JSON.stringify(userInfo);
			    			      $scope.loading = false;
			    			      
			    			      localStorage.setItem('loggedIn', true);
			    			      localStorage.setItem('clientName', $rootScope.clientName);
			    			      localStorage.setItem('userName', $rootScope.userName);
			    			      localStorage.setItem('emailId', $rootScope.emailId);
			    			      
			    		  $location.path( "/" );
			    	}else{
			    		
			    		 $scope.errorMessage = "Invalid username or password";
			    		 console.log($scope.errorMessage);
			    		 $scope.loading = false;
			    		 $scope.searchButtonText = "LOG IN";
			    		$location.path( "/login" );
			    		
			    	}
			    	
		       		   
		       		    });
			 
		
		 
		 }
	  
	  
	
	}]);