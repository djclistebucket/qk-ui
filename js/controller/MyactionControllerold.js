'use strict';

//modal controller

app.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance) {

  $scope.ok = function () {
    $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

  $scope.mytime = new Date();
});

//myaction controller

app.controller('MyactionCtrl', ['$scope', '$rootScope', '$window', '$http', '$filter', 'DateService', '$q', 'RestApiFactory', '$timeout', '$location', function ($scope, $uibModal, $log, $rootScope, $window, $http, $filter, DateService, $q, RestApiFactory, $timeout, $location) {
  console.log("MyactionCtrl");

  //var alertRadioSelectedValue="current";
  var clientName = $rootScope.clientName;
  $scope.currentAlerts = '1';
  // checkbox collapse
  $scope.isDigital = true;
  $scope.isIcare = true;
  $scope.isSpectra = true;
  $scope.isTrade = true;

  $scope.currentView = 'active_alert'; // 'schedule_downtime' , 'schedule_report'

  $scope.setActiveTab = function (val) {

    $scope.currentView = val;
  };

  $scope.callHistoryAlertRestAPI = function(clientName) {
    var urlApp = "rest/kpiservice/alerts/historical/" + clientName
    console.log("urlApp=" + urlApp);

    var appParam = {};
    appParam.clientName = clientName;
    appParam.locationName = "Mumbai-QK";
    appParam.bandwidth = "524288";

    appParam.urlApp = urlApp;


    RestApiFactory.getActiveAlerts(appParam)
      .then(function (data) {
        $scope.Activealerts = [];

        $scope.getActivealerts = data.response;
        console.log(JSON.stringify($scope.getActivealerts));
        $scope.Activealerts = $scope.getActivealerts.alerts;

        console.log(JSON.stringify($scope.Activealerts));


      });

  } // end callActiveAlertRestAPI
    var currentmilliseconds = (new Date).getTime();

    $scope.filterByActiveAlertsforLast24Hrs = function (activeAlert) {
      return (currentmilliseconds - activeAlert.errorTime >= 86400000)
    };

    $scope.filterByActiveAlertsforLast6Hrs = function (activeAlert) {
      return (currentmilliseconds - activeAlert.errorTime >= 2700000 && currentmilliseconds - activeAlert.errorTime < 21600000)
    };

    $scope.filterByActiveAlertsforLast45mins = function (activeAlert) {
      return (currentmilliseconds - activeAlert.errorTime >= 1800000 && currentmilliseconds - activeAlert.errorTime < 2700000)
    };

    $scope.filterByActiveAlertsforLast30mins = function (activeAlert) {
      return (currentmilliseconds - activeAlert.errorTime >= 900000 && currentmilliseconds - activeAlert.errorTime < 1800000)
    };

    $scope.filterByActiveAlertsforLast15mins = function (activeAlert) {
      return (currentmilliseconds - activeAlert.errorTime >= 0 && currentmilliseconds - activeAlert.errorTime < 900000)
    };



  $scope.callActiveAlertRestAPI(clientName);

  //radio button function
  $scope.processAlertRadio = function (alertRadioValue) {
    console.log("processAlertRadio function invoked with " + alertRadioValue);

    if (alertRadioValue == 'current') {
      $scope.callActiveAlertRestAPI(clientName);
    }
    else if (alertRadioValue == 'historical') {
      $scope.callActiveAlertRestAPI(clientName);
    }

  }; // end of processAlertRadio


  //stop escalation modal function

  $scope.OpenStopModal = function (size) {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'stop-escalation-modal.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve: {
        items: function () {

        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  //extend escalation modal function

  $scope.OpenExtendModal = function (size) {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'extend-escalation-modal.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve: {
        items: function () {

        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  //confirm escalation modal function

  $scope.OpenConfirmModal = function (size) {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'confirm-stop-modal.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve: {
        items: function () {

        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  //alert page preview modal function

  $scope.OpenPreviewModal = function (size) {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'alert-preview-modal.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve: {
        items: function () {

        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

}]);