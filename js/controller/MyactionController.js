'use strict';

/* This Controller is used to display MyAction Current/Historical cxguardian  */

app.controller('MyActionCtrl', ['$scope', '$rootScope','DateService','RestApiFactory', '$uibModal', '$log','$http', function ($scope, $rootScope, DateService, RestApiFactory, $uibModal, $log,$http) {

  console.log("MyAction ",$rootScope.clientName);
  console.log("userName ",$rootScope.userName);

  $scope.showalertof = '0';
  $scope.alertRadioValue = 'current';
  $scope.currentView = 'active_alert'; // 'schedule_downtime' , 'schedule_report'
  var currentmilliseconds = (new Date).getTime();
  $scope.stopEscalationReason = 'my reason';


  $scope.setActiveTab = function (val) {

    $scope.currentView = val;
  };

  $scope.callCurrentAlertRestAPI = function (clientName) {
    console.log("I am in current alerts");
    $rootScope.loader= true;
    var appParam = {};
    appParam.clientName = clientName;
    appParam.locationName = "Mumbai-QK";
    appParam.bandwidth = "524288";
    appParam.urlApp = "/rest/kpiservice/alerts/current/" + clientName;

    RestApiFactory.getActiveAlerts(appParam)
      .then(function (data) {

        $scope.Activealerts = [];
        //console.log(data.response);
        if(data.response) {

          $scope.getActivealerts = data.response;

          //console.log(JSON.stringify($scope.getActivealerts));
          $scope.Activealerts = $scope.getActivealerts.alerts;

          //console.log(JSON.stringify($scope.Activealerts));
          //console.log('records', $scope.Activealerts.length);
          $rootScope.loader= false;
        }
        if(data.statusCode == 1){
          alert(data.message);
          $rootScope.loader= false;
        }
      });

    var currentmilliseconds = (new Date).getTime();
    console.log('currentmilliseconds',currentmilliseconds);
    $scope.filterByActiveAlertsforAbove1Hrs = function (activeAlert) {
      return (currentmilliseconds - activeAlert.errorTime >= 3600000)
    };

    $scope.filterByActiveAlertsforLast1Hrs = function (activeAlert) {
      return (currentmilliseconds - activeAlert.errorTime >= 1800000 && currentmilliseconds - activeAlert.errorTime < 3600000)
    };


    $scope.filterByActiveAlertsforLast30mins = function (activeAlert) {
      return (currentmilliseconds - activeAlert.errorTime >= 900000 && currentmilliseconds - activeAlert.errorTime < 1800000)
    };

    $scope.filterByActiveAlertsforLast15mins = function (activeAlert) {
      return (currentmilliseconds - activeAlert.errorTime >= 0 && currentmilliseconds - activeAlert.errorTime < 900000)
    };

  };

//histrorical table content

  $scope.callHistroricalAlertRestAPI = function (clientName) {
    console.log("I am in histrorical alerts");
    $rootScope.loader= true;
    var appParam = {};
    appParam.clientName = clientName;
    appParam.locationName = "Mumbai-QK";
    appParam.bandwidth = "524288";
    appParam.urlApp = "/rest/kpiservice/alerts/historical/" + clientName;

    RestApiFactory.getActiveAlerts(appParam)
      .then(function (data) {

        $scope.ActiveHalerts = [];

        if(data.response) {

          $scope.getActivealerts = data.response;
          //console.log(JSON.stringify($scope.getActivealerts));
          $scope.ActiveHalerts = $scope.getActivealerts.alerts;
          $rootScope.loader= false;
          //console.log(JSON.stringify($scope.Activealerts));
        }
        if(data.statusCode == 1){
          alert(data.message);
          $rootScope.loader= false;
        }
      });
  };

  $scope.callCurrentAlertRestAPI($rootScope.clientName);


  $scope.isAlertSelected = function (alertRadioValue) {

    if(alertRadioValue == 'current'){
      //console.log('current');
      $scope.callCurrentAlertRestAPI($rootScope.clientName);
    }else if(alertRadioValue == 'histrorical'){
      //console.log('histrorical');
      $scope.callHistroricalAlertRestAPI($rootScope.clientName);
    }

  };


  $scope.showAlert=function(alertTime){

    $scope.showAlertof=alertTime;
    //console.log('showAlertof', alertTime)
  };


//Histrorical table sorting

  $scope.sort = {
    column: '',
    descending: false
  };

  $scope.changeSorting = function(column) {

    var sort = $scope.sort;

    if (sort.column == column) {
      sort.descending = !sort.descending;
    } else {
      sort.column = column;
      sort.descending = false;
    }
  };

  $scope.selectedTab = function(column) {
    return column == $scope.sort.column && 'sort-' + $scope.sort.descending;
  };

  $scope.getIcon = function(column) {

    var sort = $scope.sort;

    if (sort.column == column) {
      return sort.descending
        ? 'icon-sort-desc'
        : 'icon-sort-asc';
    }

    return 'icon-sort-default';
  };

//Stop escalation modal

  $scope.OpenStopModal = function (obj,size,type) {
    var myCtrl = this;

    /*if(type=='page'){

    }else if(type=='application'){

    }*/
    var modalInstance1 = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'stop-escalation-modal.html',
      controller: ['$scope', '$uibModalInstance',  function ($scope, $uibModalInstance) {
        //console.log('obj',obj);
        $scope.objAlert= obj;
        $scope.type = type;
        $scope.stopEscalationReason = 'my reason';
        $scope.escalStartTime = DateService.startDate(0);
        console.log( "stopstartTime="+$scope.escalStartTime);
        $scope.escalEndTime = $scope.escalStartTime;
        console.log( "stopendTime=="+$scope.escalEndTime);

        $scope.ConfirmStop = function (obj) {
         // $scope.objAlert= obj;

          obj.stopEscalationReason = $scope.stopEscalationReason;
          //console.log('reason', reason);
          obj.stopEscalationStartTime = $scope.escalStartTime;
          obj.stopEscalationEndTime = moment($scope.escalEndTime).unix()*1000;

          console.log('obj.stopEscalationEndTime', obj.stopEscalationEndTime,$scope.escalEndTime);



            $uibModalInstance.close();
            myCtrl.OpenConfirmModal(obj,size,type);
          };
          $scope.ModalCancel = function () {
            $uibModalInstance.close();
          };

          $scope.CancelStop = function (obj) {
            $scope.objAlert= obj;
            var errorTime = obj.errorTime;
            var applicationName = obj.applicationName;
            var transactionName = obj.transactionName;
            var pageName = obj.pageName;
            var user = obj.lastModifiedBy;
            myCtrl.cancelStopEscalation($rootScope.clientName,errorTime,applicationName,transactionName,pageName,user);
            //console.log('cancelStopEscalation', $scope);

            $uibModalInstance.close();
          }
      }],
      size: size
    });
  };

  $scope.OpenConfirmModal = function (obj,size,type) {
    var myCtrl = this;
    var modalInstance2 = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'confirm-stop-modal.html',
      controller: ['$scope', '$uibModalInstance',  function ($scope, $uibModalInstance) {
        $scope.objAlert= obj;
        $scope.type = type;


        $scope.stop = function (obj) {
          //console.log('stop object', obj);
          $scope.objAlert= obj;
          var errorTime = obj.errorTime;
          var applicationName = obj.applicationName;
          var transactionName = obj.transactionName;
          var pageName = obj.pageName;
          var stopEscalationStartTime = obj.stopEscalationStartTime;
          var endDate = obj.stopEscalationEndTime;
          console.log('endDate #####',obj.stopEscalationEndTime);
          var user = obj.lastModifiedBy;
          var reason = obj.stopEscalationReason;

          myCtrl.stopEscalationData($rootScope.clientName,errorTime,applicationName,transactionName,pageName,stopEscalationStartTime,endDate,reason,user);


          $uibModalInstance.close();
        };
        $scope.ModalCancel = function () {
          $uibModalInstance.close();
        };
      }],
      size: size
    });
  };

  $scope.stopEscalationData = function (clientName,errorTime,applicationName,transactionName,pageName,stopEscalationStartTime,stopEscalationEndTime,stopEscalationReason,user) {
    $rootScope.loader= true;
    console.log('stopping');
    var appParam = {};
    appParam.clientName = clientName;
    appParam.urlApp = "/rest/kpiservice/alerts/stopEscalation/" + clientName;
    appParam.locationName = "Mumbai-QK";
    appParam.bandwidth ="524288";
    appParam.errorTime = errorTime;
    appParam.applicationName = applicationName;
    appParam.transactionName = transactionName;
    appParam.pageName = pageName;
    appParam.stopEscalationStartTime = stopEscalationStartTime;
    appParam.stopEscalationEndTime = stopEscalationEndTime;
    appParam.stopEscalationReason = stopEscalationReason;
    appParam.user = user;


    RestApiFactory.myActionStopEscalation(appParam)
      .then(function (data) {

      // console.log('$scope.myActionStopEscalation.alerts',$scope.myActionStopEscalation.alerts);

        $scope.EscalationData = [];

        if(data.response) {

         $scope.myActionStopEscalation = data.response;
         //console.log(JSON.stringify("myActionStopEscalation" , $scope.myActionStopEscalation));
         $scope.EscalationData = $scope.myActionStopEscalation.alerts;

          $scope.callCurrentAlertRestAPI($rootScope.clientName);
          $rootScope.loader= false;
        }
        if(data.statusCode == 1){
          alert(data.message);
          $rootScope.loader= false;
        }
      });
  };

  $scope.cancelStopEscalation = function (clientName,errorTime,applicationName,transactionName,pageName,user) {

    console.log('cancelling');
    var appParam = {};
    appParam.clientName = clientName;
    appParam.urlApp = "/rest/kpiservice/alerts/cancelStopEscalation/" + clientName;
    appParam.locationName = "Mumbai-QK";
    appParam.bandwidth ="524288";
    appParam.errorTime = errorTime;
    appParam.applicationName = applicationName;
    appParam.transactionName = transactionName;
    appParam.pageName = pageName;
    appParam.user = user;


    RestApiFactory.cancelStopEscalation(appParam)
      .then(function (data) {

        $scope.cancelEscalation = [];

        if(data.response) {
          $scope.cancelStopEscalation = data.response;
          //console.log(JSON.stringify("myActionStopEscalation" , $scope.myActionStopEscalation));
          $scope.cancelEscalation = $scope.cancelStopEscalation.alerts;

          $scope.callCurrentAlertRestAPI($rootScope.clientName);
        }
        if(data.statusCode == 1){
          alert(data.message);
          $rootScope.loader= false;
        }
      });
  };


// Schedule Downtime all

  $scope.scheduleDowntimeData = function (clientName) {
    $rootScope.loader= true;
    console.log("I am in schedule Downtime Tab");
    var appParam = {};
    appParam.clientName = clientName;
    appParam.locationName = "Mumbai-QK";
    appParam.bandwidth = "524288";
    appParam.urlApp = "/rest/kpiservice/scheduled-downtime/all/" + clientName;

    RestApiFactory.getScheduleDowntimeAll(appParam)
      .then(function (data) {

        $scope.ScheduleDowntimeData = [];

        if(data.response) {

          $scope.getScheduleDowntimeAll = data.response;
          //console.log(JSON.stringify($scope.getActivealerts));
          $scope.ScheduleDowntimeData = $scope.getScheduleDowntimeAll.scheduledDownTime;

          //console.log(JSON.stringify($scope.Activealerts));
          $rootScope.loader= false;
        }
        if(data.statusCode == 1){
          alert(data.message);
          $rootScope.loader= false;
        }

      });

  };

  $scope.fetchDowntimeData = function () {
    $scope.scheduleDowntimeData($rootScope.clientName);
  };


//Fetch page list

  $scope.DowntimeAppList = function (clientName) {
    $rootScope.loader= true;
    var appParam = {};
    appParam.clientName = clientName;
    appParam.locationName = "Mumbai-QK";
    appParam.bandwidth = "524288";
    appParam.urlApp = "/rest/kpiservice/metadata/" + clientName;

    RestApiFactory.getDrilldownMenu(appParam).then(function (data) {

      $scope.DowntimeApplications = [];

      if(data.response){

        $scope.getDrilldownMenu = data.response;
        $scope.DowntimeApplications = $scope.getDrilldownMenu.metadataEntry;

        angular.forEach($scope.DowntimeApplications, function(appList) {
         // console.log('appList',appList);
          var appParam = {};
          appParam.clientName = clientName;
          appParam.appLocMapId = appList.appLocMapId;
          appParam.urlApp = "/rest/kpiservice/metadata/" + clientName + "/" + appList.appLocMapId;

          RestApiFactory.getDrilldownMenu(appParam).then(function (data) {

            $scope.AppPages = [];

            if (data.response) {

              $scope.getDrilldownMenu = data.response;
              //console.log('data.response', data.response, data.response.applications);
              appList.transactions = $scope.getDrilldownMenu.applications[0].transactions;
              //console.log('appList pages', appList.transactions[0].pages);

              //$scope.pageList.push(appList.transactions[0].pages);
              $rootScope.loader= false;
            }
            if(data.statusCode == 1){
              alert(data.message);
              $rootScope.loader= false;
            }
          })
        });
      }
      if(data.statusCode == 1){
        alert(data.message);
        $rootScope.loader= false;
      }
    })
  };

  $scope.DowntimeAppList($rootScope.clientName);

//Functions for check all checkboxes

  $scope.toggleAll = function() {
    angular.forEach($scope.DowntimeApplications, function(application, i) {
        angular.forEach(application.transactions, function(transaction,a) {
        angular.forEach(transaction.pages, function (page) {
          if(application.selected) {
            page.selected = true;
          }else{
            page.selected = false;
            }
          });
        });
    });
  };

  $scope.pageCheck = function() {

    angular.forEach($scope.DowntimeApplications, function(application, i) {
      angular.forEach(application.transactions, function(transaction,a) {
        angular.forEach(transaction.pages, function (page,b) {
          if(!page.selected){
            application.selected = false;
          }
        });
      });
    });
  };


//date picker
  $scope.modify = false;
  $scope.downtimeStart = DateService.startDate(0);
  $scope.downtimeEnd = $scope.downtimeStart;


// save new downtime

  $scope.saveDowntimeApplicationData = function (clientName,startTime,endTime,applicationName,userName) {
    $rootScope.loader= true;
    var appParam = {};
    appParam.clientName = clientName;
    appParam.startTime = startTime;
    appParam.endTime = endTime;
    appParam.applicationName = applicationName;
    appParam.scheduledBy = userName;
    appParam.locationName = "Mumbai-QK";
    appParam.bandwidth = "524288";
    appParam.urlApp = "/rest/kpiservice/scheduled-downtime/save/" + clientName;

    RestApiFactory.saveNewDowntime(appParam).then(function (data) {

      $scope.DowntimeForm = [];

      if (data.response) {

        $scope.saveNewDowntime = data.response;
        $scope.DowntimeForm = $scope.saveNewDowntime;

        $scope.scheduleDowntimeData($rootScope.clientName);
        $rootScope.loader= false;
      }
      if(data.statusCode == 1){
        alert(data.message);
        $rootScope.loader= false;
      }
    })

  };

  $scope.generateNewDowntime = function () {

    //console.log('applicationName',obj);
    var startTime = moment($scope.downtimeStart).unix()*1000;
    var endTime = moment($scope.downtimeEnd).unix()*1000;

    if(endTime <  new Date()){
      $scope.errMessage = 'End Date should not be less than today';
      console.log(" $scope.errMessage"+ $scope.errMessage);
      alert( $scope.errMessage);
      return false;
    }

    if(startTime >  endTime){
      $scope.errMessage = 'start Date should not be greate than End date';
      console.log(" $scope.errMessage"+ $scope.errMessage);
      alert( $scope.errMessage);
      return false;
    }

    angular.forEach($scope.DowntimeApplications, function (application,i) {
      //console.log('$scope.DowntimeApplications',$scope.DowntimeApplications);
      if (application.selected) {
        var applicationName = application.applicationName;
        $scope.saveDowntimeApplicationData($rootScope.clientName, startTime, endTime, applicationName, $rootScope.userName);
      }else{
        angular.forEach(application.transactions, function(transaction,a) {
          //console.log('transaction name',transaction.transactionName);
          angular.forEach(transaction.pages, function (page,b) {
            if (page.selected && page.selected == true) {
              console.log('Savepage',page.pageName);
              $scope.saveScheduleDowntimeByPages($rootScope.clientName, startTime, endTime, application.applicationName,page.pageName,$rootScope.userName);
            }
          });
        });
      }
    });
  };

// delete schedule downtime

  $scope.deleteScheduleDowntime = function (clientName,applicationName,pageNames) {
    $rootScope.loader= true;
    var appParam = {};
    appParam.clientName = clientName;
    appParam.applicationName = applicationName;
    appParam.pageNames = pageNames;
    appParam.locationName = "Mumbai-QK";
    appParam.bandwidth = "524288";
    appParam.urlApp = "/rest/kpiservice/scheduled-downtime/delete/" + clientName;

    RestApiFactory.DeleteDowntimebyPages(appParam).then(function (data) {

      console.log('data',data);
      $scope.DeleteForm = [];

      if (data.response) {
        console.log('data-inside',data);
        $scope.DeleteDowntime = data.response;
        $scope.DeleteForm = $scope.DeleteDowntime;

        $scope.scheduleDowntimeData($rootScope.clientName);
        $rootScope.loader= false;
      }
      if(data.statusCode == 1){
        alert(data.message);
        $rootScope.loader= false;
      }
    })
  };

  $scope.cancelSchedule = function (obj) {

    var applicationName = obj.applicationName;
    //console.log('cancel',obj.applicationName);
    $scope.deleteScheduleDowntime($rootScope.clientName,applicationName,obj.pageName);
  }

//save schedule downtime by pages
  
  $scope.saveScheduleDowntimeByPages = function (clientName,startTime,endTime,applicationName,selectedPage,userName) {

      $rootScope.loader= true;
      var appParam = {};
      appParam.clientName = clientName;
      appParam.startTime = startTime;
      appParam.endTime = endTime;
      appParam.applicationName = applicationName;
      appParam.pageNames = selectedPage;
      appParam.scheduledBy = userName;
      appParam.locationName = "Mumbai-QK";
      appParam.bandwidth = "524288";
      if($scope.modify == true){
        appParam.urlApp = "/rest/kpiservice/scheduled-downtime/update/" + clientName;
      }else {
        appParam.urlApp = "/rest/kpiservice/scheduled-downtime/save/" + clientName;
      }

    RestApiFactory.saveDowntimebyPages(appParam).then(function (data) {

        $scope.DowntimeFormbyPage = [];

        if (data.response) {

          $scope.saveDowntimebyPages = data.response;
          $scope.DowntimeFormbyPage = $scope.saveDowntimebyPages;

          $scope.scheduleDowntimeData($rootScope.clientName);
          $rootScope.loader= false;
        }
        if(data.statusCode == 1){
          alert(data.message);
          $rootScope.loader= false;
        }
      })

    };

// modify schedule downtime by pages

 /* $scope.UpdateScheduleDowntimebyPage = function (clientName,startTime,endTime,applicationName,selectedPage,userName) {

    $rootScope.loader= true;
    var appParam = {};
    appParam.clientName = clientName;
    appParam.startTime = startTime;
    appParam.endTime = endTime;
    appParam.applicationName = applicationName;
    appParam.pageNames = selectedPage;
    appParam.scheduledBy = userName;
    appParam.locationName = "Mumbai-QK";
    appParam.bandwidth = "524288";
    appParam.urlApp = "/rest/kpiservice/scheduled-downtime/update/" + clientName;

    RestApiFactory.UpdateDowntimebyPages(appParam).then(function (data) {

      $scope.ModifybyPage = [];

      if (data.response) {

        $scope.UpdateDowntimebyPages = data.response;
        $scope.ModifybyPage = $scope.UpdateDowntimebyPages;

        $scope.scheduleDowntimeData($rootScope.clientName);
        $rootScope.loader= false;
      }
      if(data.statusCode == 1){
        alert(data.message);
        $rootScope.loader= false;
      }
    })

  }*/
  
  $scope.modifyDowntime = function (obj) {

    $scope.modify = true;
    console.log('modify', $scope.modify);
    $scope.downtimeStart = obj.startTime;
    $scope.downtimeEnd = obj.endTime ;

    angular.forEach($scope.DowntimeApplications, function (application,i) {
      //console.log('$scope.DowntimeApplications',$scope.DowntimeApplications);
      if (application.applicationName == obj.applicationName) {
            //application.selected =  true;
        angular.forEach(application.transactions, function(transaction,a) {
          //console.log('transaction name',transaction.transactionName);
          angular.forEach(transaction.pages, function (page,b) {
            if (page.pageName == obj.pageName) {
              page.selected =  true;
              $scope.isDigital = true;
              //console.log('obj-page',obj.pageName);
            }
          });
        });
      }
    });
  }

}]);
