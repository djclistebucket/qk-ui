'use strict';

/* This Controller is used to display drilldown cxguardian dashboard */

app.controller('DrillDownCtrl', ['$scope', '$rootScope','DateService','RestApiFactory','$filter','TimeSeriesService', '$q','CommonUtilService','$location', '$route',  function ($scope,$rootScope,DateService,RestApiFactory,$filter,TimeSeriesService, $q,CommonUtilService,$location,$route) {

	 console.log("DrillDownCtrl reporting for duty.");
	 
	 $rootScope.stateSelected = "Transactions";
	
	 $scope.stringIsNumber = function(s) {
	        var x = +s; // made cast obvious for demonstration
	        return x.toString() === s;
	    }; 
	    
	 
	    $scope.isNumber = function(str) {
	        var pattern = /^\d+$/;
	        return pattern.test(str);  // returns a boolean
	    }
	 
	 $scope.changeData = function (state,startTime,endTime) {
		 console.log("I am in changeData"); 
		 console.log("stateSelected===="+$rootScope.stateSelected);
		 
		 console.log("startTime===="+startTime);
		 console.log("endTime===="+endTime);
		var startDate = startTime;
		console.log("startDate===="+startDate);
		var endDate = endTime;
		console.log("endDate===="+endDate);
		//check stateDate is epoch
		
		  if($scope.isNumber(startDate)){
			  console.log("input is timestamp");
		  }else{
			  console.log("input is date");
			  var d = Date.parse(startDate);
			  console.log("input is date"+d);
			  startDate  = d;
		  }
		  
		  if($scope.isNumber(endDate)){
			  console.log("input is timestamp");
		  }else{
			  console.log("input is date");
			  var d = Date.parse(endDate);
			  console.log("input is date"+d);
			  endDate = d;
		  }
		
		  
		  if(new Date(startDate) >  new Date(endDate)){
		      $scope.errMessage = 'start Date should be greate than End date';
		      console.log(" $scope.errMessage"+ $scope.errMessage);
		      alert( $scope.errMessage);
		      return false;
		    } 
		  
		  if(new Date(endDate) >  new Date()){
		      $scope.errMessage = 'End Date should be not be greate than today';
		      console.log(" $scope.errMessage"+ $scope.errMessage);
		      alert( $scope.errMessage);
		      return false;
		    } 
		  console.log("state==="+state);
		  
		  	var applicationName = localStorage.getItem('applicationName');
			 console.log("applicationName==="+applicationName);
			 
	    	 var clientName = localStorage.getItem('clientName');
	 		 console.log("clientName==="+ clientName);
	 		 
             var pageNames = [];
	    	 
	    	 
		 if($rootScope.stateSelected == "Transactions"){
		 console.log("transcationNamesSelected==="+ JSON.stringify($rootScope.transcationNamesSelected));
     	 transNames = $rootScope.transcationNamesSelected;
		 }else{
			 console.log("PageNamesSelected==="+ JSON.stringify($rootScope.PageNamesSelected));

			 pageNames = $rootScope.PageNamesSelected
		 }
     
		  
		  var postDynamicData = {};
     	 postDynamicData.locationName = "Mumbai-QK";
     	 postDynamicData.bandwidth = "524288";
     	 postDynamicData.startTime = startDate;
     	 postDynamicData.endTime =  endDate ;
     	 
     	 if($rootScope.stateSelected == "Transactions"){
     	 postDynamicData.transactionNames = transNames;
     	 }else{
     		postDynamicData.pageNames = pageNames;
     	 }
     	     	 
		  
		  if(state == "a"){
	        	state = "Availability";	
	        		 
	        	 // Application Bar data and Application Details table
	        	 
	        	
	        		     		 
	     		
	     		 if($rootScope.stateSelected == "Transactions"){	
	     		
	     			 var urlApp = "/rest/kpiservice/state/availability/"+clientName+"/"+applicationName;
		     		 console.log("urlApp==="+ urlApp);
		     		 
		     		 postDynamicData.urlApp = urlApp;
		     		
		     		 
		     		 RestApiFactory.getApplicationDrillData(postDynamicData).then(function(data)
	     		 
	     		       {
	     			    	  $scope.setDynamicData($rootScope.stateSelected,data)
	 	     		    });
		     		 
		     		// Retrieve time series data for availability
		    		 var urlApp = "/rest/kpiservice/timeseries/availability/"+clientName+"/"+applicationName;
		    		 console.log("urlApp==="+ urlApp);
		    		 postDynamicData.urlApp = urlApp;
		    		 
		    		  RestApiFactory.getApplicationDrillData(postDynamicData).then(function(data) {
							 console.log(JSON.stringify(data));
							 
							 if(data.statusCode == 1)
					    	   {
								 console.log("statusCode=== 1" );
								 $rootScope.availableChartConfig.series = [];
					    	   } else {
							 console.log(JSON.stringify(data.response.multiTimeSeries));
							 
							 $rootScope.availabilityDataSource = TimeSeriesService.timeSeries(data);
							 console.log("availabilityDataSource==="+  $rootScope.availabilityDataSource);
							 $rootScope.availableChartConfig.series =   $rootScope.availabilityDataSource
							 console.log("$scope.performanceChartConfig.series==="+  JSON.stringify($rootScope.availableChartConfig.series));
							 
					    	   }
					  });
		    		  
	     		
	     		
	     		
	     		 } else {
	     			 var transcationName = $rootScope.transcationNamesSelected.toString(); 
	     			var urlApp = "/rest/kpiservice/state/availability/"+clientName+"/"+applicationName +"/"+transcationName;
	    	 		console.log("urlApp==="+ urlApp);
	    	 		 postDynamicData.urlApp = urlApp;
	    	 		
	     			// Retrieve availability details data 
	     		 	   RestApiFactory.getPageDrillData(postDynamicData).then(function(data) {
	     					 console.log("getPageDrillData==="+JSON.stringify(data));
	     				     $scope.setDynamicData($rootScope.stateSelected,data)
	     					
	     		 	   });

	     		 }
	     		
	        }else{
	        	state = "Performance";	
	        	 // Retrieve time series data for performance
			    
	        	 if($rootScope.stateSelected == "Transactions"){
			    var urlApp = "/rest/kpiservice/timeseries/performance/"+clientName+"/"+applicationName;
				 console.log("urlApp==="+ urlApp);
				 
				 postDynamicData.urlApp = urlApp;
				 
			     RestApiFactory.getApplicationDrillData(postDynamicData).then(function(data) {
					 console.log(JSON.stringify(data));
					 if(data.statusCode == 1)
			    	   {
						 console.log("statusCode=== 1" );
						 $rootScope.performanceChartConfig.series =  [];
			    	   } else {
					
					 $rootScope.performanceDataSource = TimeSeriesService.timeSeries(data);
					 console.log("performanceDataSource==="+  $rootScope.performanceDataSource);
					 $rootScope.performanceChartConfig.series =   $rootScope.performanceDataSource
					 console.log("$scope.performanceChartConfig.series==="+  JSON.stringify($scope.performanceChartConfig.series));
			    	   }
					 
			  });
	        	 } else{
	        		 var transcationName = $rootScope.transcationNamesSelected.toString(); 
	        		 var urlApp = "/rest/kpiservice/timeseries/performance/"+clientName+"/"+applicationName + "/"+transcationName;
	 				 console.log("urlApp==="+ urlApp);
	 				 postDynamicData.urlApp = urlApp;
	 				 
	 				 RestApiFactory.getPageDrillData(postDynamicData).then(function(data) {
						 console.log(JSON.stringify(data));
						 if(data.statusCode == 1)
				    	   {
							 console.log("statusCode=== 1" );
							 $rootScope.performanceChartConfig.series = [];
							 
				    	   } else {
						 console.log(JSON.stringify(data.response.multiTimeSeries));
						 
						 $rootScope.performanceDataSource = TimeSeriesService.timeSeries(data);
						 console.log("performanceDataSource==="+  $rootScope.performanceDataSource);
						 $rootScope.performanceChartConfig.series =   $rootScope.performanceDataSource
						 console.log("$scope.performanceChartConfig.series==="+  JSON.stringify($scope.performanceChartConfig.series));
				    	   }
						 
				  });
	        		 
	        		 
	        	 }   
			
		
	        	 if($rootScope.stateSelected == "Transactions"){		
				 var urlApp = "/rest/kpiservice/state/performance/"+clientName+"/"+applicationName;
				  console.log("urlApp==="+ urlApp);
				  postDynamicData.urlApp = urlApp;
				  
				  RestApiFactory.getApplicationDrillData(postDynamicData).then(function(data) {
			 			console.log(JSON.stringify(data));
			 			if(data.statusCode == 1)
				    	   {
							 console.log("statusCode=== 1" );
							 $rootScope.performanceData = [];
				    	   } else {
			 			 console.log(JSON.stringify(data.response.performanceDetailsTable));
					  
					  var performanceDetailsTable = [];
					  performanceDetailsTable = angular.fromJson(data.response.performanceDetailsTable);
					  console.log("performanceDetailsTable=="+JSON.stringify(performanceDetailsTable));
			  	      $rootScope.performanceData = performanceDetailsTable;
					  }
				  });
				  
				
				  
		    } else {
		    	  var transcationName = $rootScope.transcationNamesSelected.toString(); 
		    	  var urlApp = "/rest/kpiservice/state/performance/"+clientName+"/"+applicationName + "/"+transcationName;
					console.log("urlApp==="+ urlApp);
					postDynamicData.urlApp = urlApp;  
			
					// Retrieve performance details data 
			 	   RestApiFactory.getPageDrillData(postDynamicData).then(function(data) {
						 console.log("getPageDrillData==="+JSON.stringify(data));
						 if(data.statusCode == 1)
				    	   {
							 console.log("statusCode=== 1" );
							 $rootScope.performanceData = [];
							 
				    	   } else { 
						 var performanceDetailsTable = [];
				 			
						 performanceDetailsTable = angular.fromJson(data.response.performanceDetailsTable);
						 console.log("performanceDetailsTable=="+ JSON.stringify(performanceDetailsTable));
						 $rootScope.performanceData  = performanceDetailsTable;
				    	   }
			 	  });
			 	   
		    }
		  
		
		 
		 
	 }
	 }
	 
	 
	 $scope.setDynamicData= function(type,data){
		 
		
		
			if(data.statusCode == 1) {
	    	      console.log("statusCode=== 1" );
				  $rootScope.availableChartConfig.series = [];
	     		  var seriesBarObj = [];
	     		  $rootScope.availabilityColumnChartConfig.series = seriesBarObj;
	     		  $rootScope.availabilityData = [];
	     		 
			 }else{
				        //var type = "Transactions"
	     		 		var seriesBarObj = TimeSeriesService.BarData(data,type);
	     		    	console.log("seriesBarObj=="+ JSON.stringify(seriesBarObj));
	     	 		    $rootScope.availabilityColumnChartConfig.series[0] = seriesBarObj;
	     	    	    console.log("seriesBarObj=="+ JSON.stringify($rootScope.availabilityColumnChartConfig.series[0]));
	     	    	    
	     	    	   var availabilityDetailsTable = [];
	     	 		   availabilityDetailsTable = angular.fromJson(data.response.availabilityDetailsTable);
	     	 		   console.log("availabilityDetailsTable=="+ JSON.stringify(availabilityDetailsTable));
	     	 		   $rootScope.availabilityData = availabilityDetailsTable;
				 
			 }
	}
		
	 
	 
	 $scope.reloadRoute = function () {
		 $route.reload();
		 };
		
		 $scope.changeDate = function(startDate){
			 console.log("I am in change Date function");
			 console.log("startDate===="+startDate);
			 
		 }
		 console.log( "startDate========="+$scope.startDate); 
		 
	
	 $rootScope.tree = [];    
	 
	
	// date and time picker
	    this.picker3 = {
	        date: new Date()
	    };
	    
	 
	 
	 $scope.startTime = DateService.startDate(24)
	 console.log( "startTime========="+$scope.startTime);
	
	 
	 $scope.startDate = DateService.epochToJsDate($scope.startTime);
	 console.log( "startDate========="+$scope.startDate);
	 
	 $scope.day = $filter('date')($scope.startTime, 'dd');
	 console.log( "$scope.day========="+$scope.day);
	 
	 $scope.mm = $filter('date')($scope.startTime, 'MMM');
	 console.log( "$scope.mm ========="+$scope.mm );
	 
	 $scope.year = $filter('date')($scope.startTime, 'yyyy');
	 console.log( "$scope.year========="+$scope.year);
	 
	 
	 $scope.startdateTime = [];
	 $scope.startdateTime =  $scope.startDate.split(' ');
	 $scope.dispalyStartDate = $scope.startdateTime[0];
	 $scope.dispalyStartTime = $scope.startdateTime[1];
	 console.log( "dispalyStartDate========="+$scope.dispalyStartDate);
	 
	 
	 
	 $rootScope.startDay = $scope.dispalyStartDate.split('-')[0];
	 $rootScope.startMonthYear = $scope.dispalyStartDate.split('-')[1] + " " + $scope.dispalyStartDate.split('-')[2];
	 console.log( "startDay========="+$scope.startDay);
	 console.log( "startMonthYear========="+$scope.startMonthYear);
	 console.log( "dispalyStartTime========="+$scope.dispalyStartTime);
	 
	 $scope.endTime = DateService.endDate()
	 console.log( "endTime========="+$scope.endTime);
	 $scope.endDate = DateService.epochToJsDate($scope.endTime);
	 console.log( "startDate========="+$scope.endDate);
	 
	 $scope.enddateTime = [];
	 $scope.enddateTime =  $scope.endDate.split(' ');
	 $scope.dispalyEndDate = $scope.enddateTime[0];
	 $scope.dispalyEndTime = $scope.enddateTime[1];
	 console.log( "dispalyEndDate========="+$scope.dispalyEndDate);
	 console.log( "dispalyEndTime========="+$scope.dispalyEndTime);
	 
	 $rootScope.endDay = $scope.dispalyEndDate.split('-')[0];
	 $rootScope.endMonthYear = $scope.dispalyEndDate.split('-')[1] + " " + $scope.dispalyEndDate.split('-')[2];
	 console.log( "endDay========="+$scope.endDay);
	 console.log( "endMonthYear========="+$scope.endMonthYear);
	 console.log( "dispalyendTime========="+$scope.dispalyEndTime);
	 
	 
	 
	 $scope.dataBarDisplay = true;
	 $scope.dataTimeSeriesDisplay = false;
	 $scope.allAvailableError = true;
     $scope.filterAvailableError = false;
     $scope.allPerfError = true;
     $scope.filterPerfError = false;
     
     $scope.expanded = false;
     $scope.name = "";
    
     $scope.toggleExpand = function(name,state){
       console.log( "name========="+name);
       console.log( "state========="+state);
       console.log( " $scope.expanded ========="+ $scope.expanded );
          $scope.name = name; 
         if($scope.expanded){
        	 
        	 console.log( " $scope.expanded ========="+ $scope.expanded );
        	 
        	 $scope.expanded = false; 
        	 
        	
  	 	   
         } else {
        	 
        	 console.log( " $scope.expanded ========="+ $scope.expanded );
        	 
        	 $scope.startTime = DateService.startDate(24)
        	 console.log( "startTime========="+$scope.startTime);
        	 
        	 $scope.endTime = DateService.endDate()
        	 console.log( "endTime========="+$scope.endTime);
        	 
        	 var transArray = [];
  		     transArray = JSON.parse(localStorage.getItem('transArray'));
  		     console.log("transArray=="+ JSON.stringify(transArray));
  		   
        	 
        	 var pageNames = [];
        	 pageNames =    CommonUtilService.getPageNames(transArray,name);
        	 
        	 var clientName = localStorage.getItem('clientName');
    	     console.log("clientName==="+ clientName);
    	     
    	     var applicationName = localStorage.getItem('applicationName');
    		 console.log("applicationName==="+applicationName);
    		 
        	 
        	
 	 		
        	 
        	var post24HrData = {};
  	 	    post24HrData.locationName = "Mumbai-QK";
  	 	    post24HrData.bandwidth = "524288";
  	 		post24HrData.startTime =  $scope.startTime;
  	 	    post24HrData.endTime =  $scope.endTime ;
  	 	    post24HrData.pageNames = pageNames;
  	 	   
  	 	    if(state == 'b'){
  	 	// Retrieve transaction level table details data for breaches
  	 	     var urlApp = "/rest/kpiservice/timeseries/breach/"+clientName+"/"+applicationName + "/" + name;
 	 		 console.log("urlApp==="+ urlApp);
  	 	     post24HrData.urlApp = urlApp;	
 	 	   RestApiFactory.getTransactionErrorData(post24HrData).then(function(data) {
 				 console.log("getTransactionErrorData==="+JSON.stringify(data));
 				  if(data.statusCode == 1) {
 		    	      console.log("statusCode=== 1" );
 		    	     $rootScope.breachData = [];
 				  } else {   
 				 var errorDetailsTable = [];
 		 			
 				errorDetailsTable = angular.fromJson(data.response.timeseries);
 				 console.log("errorDetailsTable=="+ JSON.stringify(errorDetailsTable));
 				 $rootScope.breachData = errorDetailsTable;
 				  }

 	 	   });
  	 	    } else {
  	 	     var urlApp = "/rest/kpiservice/timeseries/error/"+clientName+"/"+applicationName + "/" + name;
 	 		 console.log("urlApp==="+ urlApp);
  	 	     post24HrData.urlApp = urlApp;	
  	 	     
  	 	  // Retrieve transaction level table details data for errors
  	 	 	   RestApiFactory.getTransactionErrorData(post24HrData).then(function(data) {
  	 				 console.log("getTransactionErrorData==="+JSON.stringify(data));
  	 			  if(data.statusCode == 1) {
 		    	      console.log("statusCode=== 1" );
 		    	     $rootScope.errorData = [];
 				  } else {  
  	 				 var errorDetailsTable = [];
  	 		 			
  	 				errorDetailsTable = angular.fromJson(data.response.timeseries);
  	 				 console.log("errorDetailsTable=="+ JSON.stringify(errorDetailsTable));
  	 				 $rootScope.errorData = errorDetailsTable;
 				  }
  	 	 	   });
  	 	 	   
  	 	    	
  	 	    }
 	 	   
        	 $scope.expanded = true; 
         }
    	  
      
     }
     
	
	 $scope.singleModel = 1;
     $scope.radioModel = 'AGGREGATE';
     $scope.rdAvailability = 'AvailableAll';
     $scope.rdPerformance = 'PerfAll';

	  $scope.checkModel = {
	    left: false,
	    middle: true,
	    right: false
	  };
	  
	  $scope.toggleMode = function(radioModel) {
		
	    if (radioModel == 'AGGREGATE') {
	    	//  console.log('Left clicked')
	    	 $scope.dataBarDisplay = true;
		   	 $scope.dataTimeSeriesDisplay = false;
	    	
	    }
	    else if (radioModel == 'TIME-SERIES') {
	      //console.log('Right clicked')
	    	 $scope.dataBarDisplay = false;
	    	 $scope.dataTimeSeriesDisplay = true;
	     
	    } else if (radioModel == 'AvailableAll') {
		      console.log('AvailableAll clicked')
		      $scope.allAvailableError = true;
		       $scope.filterAvailableError = false;
	    
	    } else if (radioModel == 'AvailableError') {
		      console.log('AvailableError clicked')
		      $scope.allAvailableError = false;
		       $scope.filterAvailableError = true;
	    
	    } else if (radioModel == 'PerfAll') {
		      console.log('PerfAll clicked');
		      $scope.allPerfError = true;
		       $scope.filterPerfError = false;
	    
	    } else if (radioModel == 'PerfBreach') {
		      console.log('PerfBreach clicked');
		      $scope.allPerfError = false;
		       $scope.filterPerfError = true;
		       
	    
	    }
	  }
	 
	 

	//toggle divs up down
	 
  $scope.currentSection = localStorage.getItem('state')
   console.log("currentSection==="+ $scope.currentSection);
  
	$scope.toggleSection = function(name){
		$scope.currentSection = name;
	};
	$scope.sections = [
		{
			id:1,
			name:'Availability',
			url:'partial/Availability.html'

		},{
			id:2,
			name:'Performance',
			url:'partial/Performance.html'

		}
	];

	
	 var applicationName = localStorage.getItem('applicationName');
	 console.log("applicationName==="+applicationName);
	 
	 $rootScope.selectedApplication = applicationName;
	 
	 var appList = [];
	 appList  =  localStorage.getItem('appsList');
	 console.log("appList==="+ appList);
	 
	$rootScope.appList1 = appList.split(',');
	// console.log("$scope.appList==="+$rootScope.appList1);
	 

	 

	
    	
	 
	 $rootScope.selectedApplication;
	    $rootScope.dropboxitemselected = function (item) {
	         $rootScope.selectedApplication = item ;
	         applicationName =  $rootScope.selectedApplication;
	         console.log("applicationName==="+ applicationName);
	         var appMetaDataArray = [];
			 
			 appMetaDataArray = JSON.parse(localStorage.getItem('appMetaData'));
			 console.log("appMetaData==="+ appMetaDataArray.length);
			 
	         CommonUtilService.setAppData(appMetaDataArray,applicationName);
	    
	    	 
	    	 var appParam = {};
    	     var appLocMapId = localStorage.getItem('appLocMapId');
    	     console.log("appLocMapId==="+ appLocMapId);
    	     var clientName = localStorage.getItem('clientName');
    	     console.log("clientName==="+ clientName);
    	     var urlApp = "/rest/kpiservice/metadata/"+clientName+"/"+appLocMapId;
    	     appParam.urlApp = urlApp;
    	     console.log("urlApp==="+ appParam.urlApp);
    	     
    	     var promise1 = RestApiFactory.getDrilldownMenu(appParam).then(function(data)
 	       		    {
 	       		
 			       		if(data.statusCode == 1)
 				    	   {
 				    	 console.log("statusCode=== 1" );
 				    	  
 				    	  //console.log("Before calling drill down" );
 				    	
 				    		 var treeArray = [];
 	 				    	   treeArray =  localStorage.setItem('treeArray', treeArray);
 	 				    	   $rootScope.tree  = treeArray;
 	 				    	 $rootScope.availabilityData = [];
 	 				    	 $rootScope.performanceData
 	 				    	 $scope.reloadRoute();
 				    	   
 				    	   
 				    	   } else{
 				    		  console.log("statusCode=== 0" );
 				    		 CommonUtilService.setTransactionData(data);
 				    		// treeArray =  localStorage.getItem('treeArray', treeArray);
 				    		// $rootScope.tree  = treeArray;
 				    		 $rootScope.transactionDisplay = true;
 				    		$scope.reloadRoute();
 				    		
 				    	   }
 	       		    });
	    	 
	    	 
	         
	    }	   
	    
	    var flag = $rootScope.transactionDisplay;
	    console.log("transactionDisplay=="+ flag);
	    
	    if($rootScope.transactionDisplay == undefined){
	    	flag = true;	
	    }
	    
	    if(flag){
		
		 // Retrieving transaction names from local storage
		   var transArray = [];
		   transArray = JSON.parse(localStorage.getItem('transArray'));
		   console.log("transArray=="+ JSON.stringify(transArray));
		   var treeArray = [];
   		   treeArray =  localStorage.getItem('treeArray', treeArray);
   		   console.log("treeArray=="+ JSON.stringify(treeArray));
		  // $rootScope.tc.tree = treeArray;
   		   

		   var transNames = [];
		   $rootScope.allTransactionsArray = [];
		   angular.forEach(transArray, function(transctionItem){
			   var tranItem = transctionItem;	
			   console.log("transactionId=== "+tranItem.transactionId );	
			   console.log("transactionName=== "+tranItem.transactionName );
				var transactionName = tranItem.transactionName;
				var transElement =  transactionName;
				transNames.push(transElement);
				$rootScope.allTransactionsArray.push(transElement);
		  });
		 console.log("transNames=== "+transNames );	
		 console.log("allTransactionsArray=== "+$rootScope.allTransactionsArray );
		 
	 
	
	 	var clientName = localStorage.getItem('clientName');
		console.log("clientName==="+ clientName);
		 
	 	var urlApp = "/rest/kpiservice/state/availability/"+clientName+"/"+applicationName;
		console.log("urlApp==="+ urlApp);
		
		 $scope.startTime = DateService.startDate(24)
   	    console.log( "startTime========="+$scope.startTime);
   	  
   	    $scope.endTime = DateService.endDate()
   	    console.log( "endTime========="+$scope.endTime);
   	    
	 
	    var post24HrData = {};
	    post24HrData.locationName = "Mumbai-QK";
	    post24HrData.bandwidth = "524288";
		post24HrData.startTime =  $scope.startTime;
	    post24HrData.endTime =  $scope.endTime ;
	    post24HrData.urlApp = urlApp;
	    post24HrData.transactionNames = transNames;
	    

	 // Application Bar data and Application Details table
		
		RestApiFactory.getApplicationDrillData(post24HrData).then(function(data)
		 
		    {
			
			if(data.statusCode == 1)
	    	   {
	    	 console.log("statusCode=== 1" )
	    	 availabilityDetailsTable = [];
	    	 $rootScope.availabilityColumnChartConfig.series[0] = [];
	    	 
	    	   }else{
			 
			  var availabilityDetailsTable = [];
 			
			 availabilityDetailsTable = angular.fromJson(data.response.availabilityDetailsTable);
 		console.log("availabilityDetailsTable=="+ JSON.stringify(availabilityDetailsTable));
 		
 		if(availabilityDetailsTable == undefined ) {
 			availabilityDetailsTable = [];
 		}
 		
 	
 		
 		$rootScope.availabilityData = availabilityDetailsTable;
 		
 		 
	    	var type = "Transactions"
	 		var seriesBarObj = TimeSeriesService.BarData(data,type);
	    	console.log("seriesBarObj=="+ JSON.stringify(seriesBarObj));
 		    $rootScope.availabilityColumnChartConfig.series[0] = seriesBarObj;
    	    console.log("seriesBarObj=="+ JSON.stringify($rootScope.availabilityColumnChartConfig.series[0]));  
    		
	    	   }
 		
		    });
		
		// Retrieve time series data for availability
		 var urlApp = "/rest/kpiservice/timeseries/availability/"+clientName+"/"+applicationName;
		 console.log("urlApp==="+ urlApp);
		  
		    var post24HrData = {};
		    post24HrData.locationName = "Mumbai-QK";
		    post24HrData.bandwidth = "524288";
			post24HrData.startTime =  $scope.startTime;
		    post24HrData.endTime = $scope.endTime ;
		    post24HrData.urlApp = urlApp;
		    post24HrData.transactionNames = transNames;
		  
		   
		    RestApiFactory.getApplicationDrillData(post24HrData).then(function(data) {
				 console.log(JSON.stringify(data));
				 
				 if(data.statusCode == 1)
		    	   {
					 console.log("statusCode=== 1" );
					 $rootScope.availableChartConfig.series = [];
		    	   } else {
				 console.log(JSON.stringify(data.response.multiTimeSeries));
				 var type="availability";
				 $rootScope.availabilityDataSource = TimeSeriesService.timeSeries(data,type);
				 console.log("availabilityDataSource==="+  $rootScope.availabilityDataSource);
				 $rootScope.availableChartConfig.series =   $rootScope.availabilityDataSource
				 console.log("$scope.performanceChartConfig.series==="+  JSON.stringify($rootScope.availableChartConfig.series));
				 
		    	   }
		  });
		    
		 
		 // Retrieve time series data for performance
		    
		    var urlApp = "/rest/kpiservice/timeseries/performance/"+clientName+"/"+applicationName;
			 console.log("urlApp==="+ urlApp);
			  
			    var post24HrData = {};
			    post24HrData.locationName = "Mumbai-QK";
			    post24HrData.bandwidth = "524288";
				post24HrData.startTime =  $scope.startTime;
			    post24HrData.endTime = $scope.endTime ;
			    post24HrData.urlApp = urlApp;
			    post24HrData.transactionNames = transNames;
			  
			   
			    RestApiFactory.getApplicationDrillData(post24HrData).then(function(data) {
					 console.log(JSON.stringify(data));
					 if(data.statusCode == 1)
			    	   {
						 console.log("statusCode=== 1" );
						 $rootScope.availableChartConfig.series =  [];
			    	   } else {
					 console.log(JSON.stringify(data.response.multiTimeSeries));
					 var type="performance";
					 $rootScope.performanceDataSource = TimeSeriesService.timeSeries(data,type);
					 console.log("performanceDataSource==="+  $rootScope.performanceDataSource);
					 $rootScope.performanceChartConfig.series =   $rootScope.performanceDataSource
					 console.log("$scope.performanceChartConfig.series==="+  JSON.stringify($scope.performanceChartConfig.series));
			    	   }
					 
			  });
			    
			    
		    
		
		var urlApp = "/rest/kpiservice/state/performance/"+clientName+"/"+applicationName;
		  console.log("urlApp==="+ urlApp);
		  post24HrData.urlApp = urlApp;
		  
		  RestApiFactory.getApplicationDrillData(post24HrData).then(function(data) {
	 			 console.log(JSON.stringify(data));
	 			 
	 			if(data.statusCode == 1)
		    	   {
					 console.log("statusCode=== 1" );
					 $rootScope.performanceChartConfig.series = [];
		    	   } else {
	 			 console.log(JSON.stringify(data.response.performanceDetailsTable));
		  
		  var performanceDetailsTable = [];
		
		  performanceDetailsTable = angular.fromJson(data.response.performanceDetailsTable);
  		console.log("performanceDetailsTable=="+JSON.stringify(performanceDetailsTable));
  		  		
  		if ( performanceDetailsTable == undefined ){
  			performanceDetailsTable = []
  		}
  		
  		$rootScope.performanceData = performanceDetailsTable;
		    	   }
		  });
		  
		  
	    }
		  
		  
		
	   
		
	    
		  
		  //Chart initigration for LINE chart
  		$rootScope.availabilityColumnChartConfig = {
  				
  				options: {
  	  				chart: {
  	  					type: 'column',
  	  					marginTop:20,
  	  				},
  	  					credits: {enabled: false},
  	  					title: '',
  	  					tooltip: {enabled: false},
  	  				legend:{ enabled:false },
  	  			},
  					      xAxis: {
  				            type: 'category'
  				        },
  				      yAxis: {
  		  				min:0,
  		  				max:100,
  		  				tickInterval:10,
  		  				lineColor: '#cccccc',
  		  				lineWidth: 1,
  		  				gridLineWidth: 1,
  		  				gridLineDashStyle: 'longdash',
  		  				gridLineColor: '#ffc6bf',
  		  				title: {
  		  					align: 'high',
  		  					offset: 0,
  		  					rotation: 0,
  		  					y: -10,
  		  					text: '(%)',
  		  					style:{'color':'#000'}
  		  				},
  		  			},
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        
  		   series: [{
  					        	name: 'Transactions',
  					        	 colorByPoint: true,
  					        	data: []
  					        }],
  					        title: {
  					            text: ''
  					        },
  					        loading: false
  					   
  				
  			    };
  		
  		
  		 //Chart initigration for LINE chart
  		$rootScope.performanceChartConfig = {
  			options: {
  				chart: {
  					type: 'line',
  					marginTop: 20,
  					marginRight:320
  				},
  				legend: {
  					layout: 'vertical',
  					align: 'right',
  					verticalAlign: 'middle',
  					borderWidth: 0,
  					symbolHeight: 20,
  					symbolPadding: 14,
  					symbolWidth: 0,
  					itemStyle:{
  						'font-size': '14px',
  						'font-family': 'Helvetica, Arial, sans-serif',
  		 				'font-weight':'100'
  					},
  					itemMarginBottom: 10,
  				},
  			},
  			xAxis: {
  				type:'datetime',
  				tickWidth: 0,
  				//tickInterval: 24 * 60 * 3600 * 1000, // one day
  				tickInterval: 3600 * 1000,
  				minTickInterval: 3600 * 1000,
  				lineColor: '#cccccc',
  				lineWidth: 1,
  				/*			title: {
  				 align: 'middle',
  				 offset: -15,
  				 text: 'Sec'
  				 },*/

  			},
  			yAxis: {
  				lineColor: '#cccccc',
  				lineWidth: 1,
  				gridLineWidth: 0,
  				title: {
  					align: 'high',
  					offset: 0,
  					rotation: 0,
  					y:  -10,
  					text: '',
  					style:{'color':'#000'}
  				},
  				crosshair: {
  					width: 1.5,
  					color: '#89c657',
  					dashStyle: 'ShortDash'
  				}
  			},

  			series: [],
  			
  			 title: {
 				text: ''
 			},

 			func: function (chart) {
 				$scope.$evalAsync(function () {
 					chart.reflow();
 					//The below is an event that will trigger all instances of charts to reflow
 					//$scope.$broadcast('highchartsng.reflow');
 				});
 			},

 			loading: false
  		}
  		
  		
  		//availability time series chart configuation
  		 //Chart initigration for LINE chart
  		$rootScope.availableChartConfig = {
  			options: {
  				chart: {
  					type: 'line',
  					marginTop: 20,
  					marginRight:320
  				},
  				legend: {
  					layout: 'vertical',
  					align: 'right',
  					verticalAlign: 'middle',
  					borderWidth: 0,
  					symbolHeight: 20,
  					symbolPadding: 14,
  					symbolWidth: 0,
  					itemStyle:{
  						'font-size': '14px',
  						'font-family': 'Helvetica, Arial, sans-serif',
  		 				'font-weight':'100'
  					},
  					itemMarginBottom: 10,
  				},
  			},
  			xAxis: {
  				type:'datetime',
  				tickWidth: 0,
  				//tickInterval: 24 * 60 * 3600 * 1000, // one day
  				tickInterval: 3600 * 1000,
  				minTickInterval: 3600 * 1000,
  				lineColor: '#cccccc',
  				lineWidth: 1,
  				/*			title: {
  				 align: 'middle',
  				 offset: -15,
  				 text: 'Sec'
  				 },*/

  			},
  			yAxis: {
  				lineColor: '#cccccc',
  				lineWidth: 1,
  				gridLineWidth: 0,
  				title: {
  					align: 'high',
  					offset: 0,
  					rotation: 0,
  					y:  -10,
  					text: '',
  					style:{'color':'#000'}
  				},
  				crosshair: {
  					width: 1.5,
  					color: '#89c657',
  					dashStyle: 'ShortDash'
  				}
  			},
  			
  			series: [],
  			
  	      title: {
				text: ''
			},

			func: function (chart) {
				$scope.$evalAsync(function () {
					chart.reflow();
					//The below is an event that will trigger all instances of charts to reflow
					//$scope.$broadcast('highchartsng.reflow');
				});
			},

			loading: false
  		}
  		
  	//Chart initigration

  		$scope.coloumnChartConfig = {
  			options: {
  				chart: {
  					type: 'column',
  					marginTop:20,
  				},
  					credits: {enabled: false},
  					title: '',
  					tooltip: {enabled: false},
  				legend:{ enabled:false },
  			},
  			series: [{
  				dataLabels: {
  					enabled: true,
  					format: '{point.y}%'
  				},
  				data: [{
  					name:'Home Page...',
  					y:40,
  					color: {
  						linearGradient: { x1: 0, x2: 1, y1: 0, y2: 0 },
  						stops: [
  							[0, '#c76719'],
  							[1, '#f67f1f']
  						]
  					}
  				},{
  					name:'Some text here',
  					y:25,
  					color: {
  						linearGradient: { x1: 0, x2: 1, y1: 0, y2: 0 },
  						stops: [
  							[0, '#a81f10'],
  							[1, '#d94839']
  						]
  					}
  				},{
  					name:'Some text here',
  					y:62,
  					color: {
  						linearGradient: { x1: 0, x2: 1, y1: 0, y2: 0 },
  						stops: [
  							[0, '#4b9012'],
  							[1, '#88c555']
  						]
  					}
  				},{
  					name:'Some text here',
  					y:82,
  					color: {
  						linearGradient: { x1: 0, x2: 1, y1: 0, y2: 0 },
  						stops: [
  							[0, '#4b9012'],
  							[1, '#88c555']
  						]
  					}
  				}]
  			}],
  			xAxis: {
  				type: 'category',

  			},

  			yAxis: {
  				min:0,
  				max:100,
  				tickInterval:10,
  				lineColor: '#cccccc',
  				lineWidth: 1,
  				gridLineWidth: 1,
  				gridLineDashStyle: 'longdash',
  				gridLineColor: '#ffc6bf',
  				title: {
  					align: 'high',
  					offset: 0,
  					rotation: 0,
  					y: -10,
  					text: '(%)',
  					style:{'color':'#000'}
  				},
  			},
  			func: function (chart) {
  				$scope.$evalAsync(function () {
  					chart.reflow();
  					//The below is an event that will trigger all instances of charts to reflow
  					//$scope.$broadcast('highchartsng.reflow');
  				});
  			},
  			loading: false
  		};
  		/// line chart
  		$scope.performDetailsChartConfig = {
  			options: {
  				chart: {
  					type: 'line',
  					marginTop: 20,
  					marginRight:320
  				},
  				legend: {
  					layout: 'vertical',
  					align: 'right',
  					verticalAlign: 'middle',
  					borderWidth: 0,
  					symbolHeight: 20,
  					symbolPadding: 14,
  					symbolWidth: 0,
  					itemStyle:{
  						'font-size': '14px',
  						'font-family': 'Helvetica, Arial, sans-serif',
  		 				'font-weight':'100'
  					},
  					itemMarginBottom: 10,
  				},
  				/*
  				legend: {
  					align: 'right',
  					verticalAlign: 'top',
  					layout: 'vertical',
  					x: 0,
  					y: 100
  				},*/
  			},
  			xAxis: {
  				type:'datetime',
  				tickWidth: 0,
  				//tickInterval: 24 * 60 * 3600 * 1000, // one day
  				tickInterval: 3600 * 1000,
  				minTickInterval: 3600 * 1000,
  				lineColor: '#cccccc',
  				lineWidth: 1,
  				/*			title: {
  				 align: 'middle',
  				 offset: -15,
  				 text: 'Sec'
  				 },*/

  			},
  			yAxis: {
  				lineColor: '#cccccc',
  				lineWidth: 1,
  				gridLineWidth: 0,
  				title: {
  					align: 'high',
  					offset: 0,
  					rotation: 0,
  					y:  -10,
  					text: 'Freq',
  					style:{'color':'#000'}
  				},
  				crosshair: {
  					width: 1.5,
  					color: '#89c657',
  					dashStyle: 'ShortDash'
  				}
  			},
  			series: [{
  				name:'Some page here',
  				data: [0, 0, 0, 0, 0,0, 0, 0, 0, 0],
  				pointStart: Date.UTC(2010, 0, 1),
  				pointInterval: 24 * 3600 * 1000, // one day
  				marker: { lineWidth: 2 }

  			}],
  			title: {
  				text: ''
  			},

  			func: function (chart) {
  				$scope.$evalAsync(function () {
  					chart.reflow();
  					//The below is an event that will trigger all instances of charts to reflow
  					//$scope.$broadcast('highchartsng.reflow');
  				});
  			},

  			loading: false
  		}
  		//
  		$scope.performDetailsChartConfig.series  = [
  				{
  				name:'Home Page Login Page',
  				data: [ 7, 8, 23, 28, 29, 31, 44, 50, 54, 59, 64, 74],
  				color:'#fec6f8',
  				marker: { lineWidth: 2 }
  			},

  			{
  				name:'Customer Welcome Page After Login',
  				data: [35, 47, 52, 54, 56, 58, 59, 61, 66, 78, 79, 80],
  				color:'#f8bab3',
  				marker: { lineWidth: 2 }
  			},
  			{
  				name:'Policy List Page Post Login',
  				data: [1, 14, 20, 23, 33, 48, 59, 60, 70, 77, 78, 79],
  				color:'#bbe4f3',
  				marker: { lineWidth: 2 }
  			},
  			{
  				name:'Mode Of Payment Page LA MQ Use..',
  				data: [7, 12, 14, 16, 19, 22, 28, 50, 55, 60, 66, 77],
  				color:'#afefe8',
  				marker: { lineWidth: 2 }
  			}
  		]
  		
  		
  		//$scope.performanceChartConfig.series[0].data = [10, 15, 12, 8, 7,10, 15, 12, 8, 7] ;
  		
  		//$scope.perfData =  [10, 15, 12, 8, 7,10, 15, 12, 8, 7];

  	/*	$scope.performanceChartConfig.series[0]  = {
  			regression: true ,
  			regressionSettings: {
  				type: 'linear',
  				color:  'rgba(223, 83, 83, .9)'
  			},
  			data: $scope.performanceChartConfig.series[0].data,
  			//pointStart: Date.UTC(2010, 0, 1),
  			//pointInterval: 24 * 3600 * 1000, // one day
  			color:'#136b82',
  			marker: { lineWidth: 2 }
  		}*/
  		
  	
  //	$scope.availabilityColumnChartConfig.series[0].data[0] =  $scope.aggregateBarData;
	
	 
/*

	// toggle divs up down
	 $scope.dataBarDisplay = true;
	 $scope.dataTimeSeriesDisplay = false;
	 $scope.allDataError = true;
	 $scope.filterDataError = false;
	  $scope.allDataBreach = true;
      $scope.filterDataBreach = false;
	 
	 $scope.availabilityDataSource = {};
	 
	 $scope.aggregateAction = function() {
	        console.log("aggregateAction function invoked");
	        $scope.dataBarDisplay = true;
	   	    $scope.dataTimeSeriesDisplay = false;
	   
	  }
	 
	 $scope.timeseriesAction = function() {
	        console.log("timeseriesAction function invoked");
	        $scope.dataBarDisplay = false;
	   	    $scope.dataTimeSeriesDisplay = true;
	   	   
	   	  
	  }
	 
	 $scope.allAvailAction = function() {
	        console.log("allAvailAction function invoked");
	        $scope.allDataError = true;
	        $scope.filterDataError = false;
	   
	  }
	 
	 $scope.filterAvailAction = function() {
	        console.log("filterAvailAction function invoked");
	        $scope.allDataError = false;
	        $scope.filterDataError = true;
	       
	   
	  }
	 
	 $scope.allPerfAction = function() {
	        console.log("allPerfAction function invoked");
	        $scope.allDataBreach = true;
	        $scope.filterDataBreach = false;
	   
	  }
	 
	 $scope.filterPerfAction = function() {
	        console.log("filterPerfAction function invoked");
	        $scope.allDataBreach = false;
	        $scope.filterDataBreach = true;
	       
	   
	  }
	 
	
	 
	 
	 var applicationName = $rootScope.applicationName
	 console.log("applicationName==="+applicationName);
	 
	 $rootScope.selectedApplication = applicationName;
	 
	 var appList = [];
	 
	 appList  =  localStorage.getItem('appsList');
	 
	 console.log("appList==="+appList);
	 
	 $rootScope.appList1 = appList.split(',');
		 
	 console.log("$scope.appList==="+$rootScope.appList1);
	 
	 var appMetaDataArray = [];
	 
	 appMetaDataArray = JSON.parse(localStorage.getItem('appMetaData'));
	 console.log("appMetaData==="+ appMetaDataArray.length);
	 
	 angular.forEach(appMetaDataArray, function(item){
		 
		 var appObject = item;
		 console.log("appObject==="+ JSON.stringify(appObject));
		 var keepGoing = true;
		 angular.forEach(appObject, function(item2){
			// console.log("applicationName==="+ appObject.applicationName);
			 var appName = appObject.applicationName;
			 if(appName == applicationName){
				// console.log("applicationName found ");
				 var appLocMapId = appObject.appLocMapId;
				// console.log("appLocMapId ==== "+appLocMapId);
				 localStorage.setItem('appLocMapId', appLocMapId);
				 localStorage.setItem('applicationName', applicationName);
				 keepGoing = false;
			 }
		 });
  		
  		
	 });
	 
	 
	 
			
				 var treeArray = [];
				  transArray = localStorage.getItem('transArray');
				  console.log("transArray==="+ transArray);
				  var clientName = localStorage.getItem('clientName');
				 console.log("clientName==="+ clientName);
				 
				 var applicationName = localStorage.getItem('applicationName');
				 console.log("applicationName==="+ applicationName);
				  
				 var urlApp = "/rest/kpiservice/state/availability/"+clientName+"/"+applicationName;
				console.log("urlApp==="+ urlApp);
				 
			    
			    var transArray = [];
			    
			   transArray = JSON.parse(localStorage.getItem('transArray'));
			   
			   var transNames = [];
			   
			  angular.forEach(transArray, function(transctionItem){
				
				 var tranItem = transctionItem;	
				 console.log("transactionId=== "+tranItem.transactionId );	
					console.log("transactionName=== "+tranItem.transactionName );
					var transactionName = tranItem.transactionName;
					var transElement =  transactionName;
					transNames.push(transElement);
			  });
			  
			 console.log("transNames=== "+transNames );	
			 
			var post24HrData = {};
		    post24HrData.locationName = "Mumbai-QK";
		    post24HrData.bandwidth = "524288";
			post24HrData.startTime = "1467225000000";
		    post24HrData.endTime = "1467311399000";
		    post24HrData.urlApp = urlApp;
		    post24HrData.transactionNames = transNames;
			    
		  // Application Bar data and Application Details table
		
			 		RestApiFactory.getApplicationDrillData(post24HrData).then(function(data)
					 
					    {
			 			
			 			 
			 			  var availabilityDetailsTable = [];
			    			
			 			 availabilityDetailsTable = angular.fromJson(data.response.availabilityDetailsTable);
			    		console.log("availabilityDetailsTable=="+ JSON.stringify(availabilityDetailsTable));
			    		
			    		if(availabilityDetailsTable == undefined ) {
			    			availabilityDetailsTable = [];
			    		}
			    		var obj1 = {"name":"Mobile_Login1","numberOfRuns":9,"availablity":100,"numberOfErrors":1};
			    		
			    		var obj2 = {"name":"Mobile_Login1","numberOfRuns":9,"availablity":100,"numberOfErrors":5};
			    		
			    		var obj3 = {"name":"Mobile_Login1","numberOfRuns":9,"availablity":100,"numberOfErrors":6};
			    		
			    		var obj4 = {"name":"Mobile_Login1","numberOfRuns":9,"availablity":100,"numberOfErrors":7};
			    		
			    
			    		availabilityDetailsTable.push(obj1);
			    		availabilityDetailsTable.push(obj2);
			    		availabilityDetailsTable.push(obj3);
			    		availabilityDetailsTable.push(obj4);
			    		
			    		
			    		var availabilityBarDetails = [];
			    		
			    		availabilityBarDetails = angular.fromJson(data.response.aggregateBarChart);
				    	console.log("availabilityBarDetails=="+availabilityBarDetails);
				 			
				    		
			    		
			    		$rootScope.availabilityData = availabilityDetailsTable;
			    		
			    		$scope.aggregateBarData  = [];
			    		
			    		angular.forEach(availabilityBarDetails, function(availabilityBarItem){
			    			
			    			var value = availabilityBarItem.value;
			    			var lable = availabilityBarItem.label;
			    			var color = "";
			    			if(value >= 70){
			    				
			    				color = "#97BB2E";
			    			} else if (value >= 50){
			    				
			    				color = "#F98020";
			    			} else {
			    				
			    				color = "#DB4939";
			    			}
			    			
			    			var barDataObj = {
				    				   "label" : lable,
				    				   "value" : value,
				    				   "color" : color
				    			  }
				    		   
			    			$scope.aggregateBarData.push(barDataObj);
			    			
			    		});
			    		
			    		console.log("aggregateBarData=="+ JSON.stringify($scope.aggregateBarData));  
			    		   
			    		var barcharData = {};
			    		
			    		  var chartAttributeData = {
			    				  
			    				  "chart": {
			        			        "yaxisname": "Sales",
			        			        "bgcolor": "FFFFFF",
			        			        "useroundedges": "1",
			        			        "showPercentValues": "1",
			        			        "showborder": "0",
			        			        "plottooltext": "$label	: $$value",
			        			        "xAxisname": "Pages",
			        	                "yAxisName": "%",
			        			    }
			    		  };
			    		  
			    		  $.extend(barcharData,chartAttributeData);
			    		  console.log("barcharData=="+ JSON.stringify(barcharData));
			    		  
			    		  var chartData = {
			    				  "data" : $scope.aggregateBarData
			    		  } 
			    		  
			    		  $.extend(barcharData,chartData);
			    		  
			    		  console.log("barcharData=="+ JSON.stringify(barcharData));
			    		  
			    		  $scope.myDataSource = barcharData;
			    		
			
			});
			
	 
			  var urlApp = "/rest/kpiservice/timeseries/availability/"+clientName+"/"+applicationName;
			  console.log("urlApp==="+ urlApp);
			  
			    var post24HrData = {};
			    post24HrData.locationName = "Mumbai-QK";
			    post24HrData.bandwidth = "524288";
				post24HrData.startTime = "1467225000000";
			    post24HrData.endTime = "1467311399000";
			    post24HrData.urlApp = urlApp;
			    post24HrData.transactionNames = transNames;
			  
			   
			    RestApiFactory.getApplicationDrillData(post24HrData).then(function(data) {
					 console.log(JSON.stringify(data));
					 console.log(JSON.stringify(data.response.multiTimeSeries));
					 
					 $rootScope.availabilityDataSource = TimeSeriesService.timeSeries(data);
					 console.log("availabilityDataSource==="+  $rootScope.availabilityDataSource);
					
					 
			  });
			    
			   
			    var urlApp = "/rest/kpiservice/timeseries/performance/"+clientName+"/"+applicationName;
				console.log("urlApp==="+ urlApp);
				  
				 RestApiFactory.getApplicationDrillData(post24HrData).then(function(data) {
					 console.log(JSON.stringify(data));
					 console.log(JSON.stringify(data.response.multiTimeSeries));
					 
					 $rootScope.performanceDataSource = TimeSeriesService.timeSeries(data);
					 console.log("performanceDataSource==="+  $rootScope.performanceDataSource);
					
					 
			  });
				 
		
			    
			
			  	    		


			 
			  var urlApp = "/rest/kpiservice/state/performance/"+clientName+"/"+applicationName;
			  console.log("urlApp==="+ urlApp);
			  post24HrData.urlApp = urlApp;
			  
			  RestApiFactory.getApplicationDrillData(post24HrData).then(function(data) {
		 			 console.log(JSON.stringify(data));
		 			 console.log(JSON.stringify(data.response.performanceDetailsTable));
			  
			  var performanceDetailsTable = [];
  			
			  performanceDetailsTable = angular.fromJson(data.response.performanceDetailsTable);
	    		console.log("performanceDetailsTable=="+JSON.stringify(performanceDetailsTable));
	    		
	    		var obj1 = {"name":"Mobile_Login1","numberOfRuns":9,"averageResponseTime":20624.333333333332,"breach":1}
	    		var obj2 = {"name":"Mobile_Login2","numberOfRuns":9,"averageResponseTime":20624.333333333332,"breach":20}
	    		var obj3 = {"name":"Mobile_Login3","numberOfRuns":9,"averageResponseTime":20624.333333333332,"breach":5}
	    		var obj4 = {"name":"Mobile_Login4","numberOfRuns":9,"averageResponseTime":20624.333333333332,"breach":8}
	    		
	    		if ( performanceDetailsTable == undefined ){
	    			performanceDetailsTable = []
	    		}
	    		performanceDetailsTable.push(obj1);
	    		performanceDetailsTable.push(obj2);
	    		performanceDetailsTable.push(obj3);
	    		performanceDetailsTable.push(obj4);
	    		
	    		
	 			
	    		$rootScope.performanceData = performanceDetailsTable;
	    		
			  });
			  
				  
	 
	 
	   
   $rootScope.performanceData = [
  	        	                { "name" : "Customer welcome page after login","numberOfRuns" : 22, "averageResponseTime" : 6.8, "breach" : 6.8},
  	        	                { "name" : "Policy List Page Post Login","numberOfRuns" : 22, "averageResponseTime" : 8, "breach" : 8},
  	        	                { "name" : "Mode of Payment Page LA MQ Used Post Login","numberOfRuns" : 22, "averageResponseTime" : 2.8, "breach" : 2.8},
  	        	                { "name" : "Split Payment information Page LA MQ Used","numberOfRuns" : 22, "averageResponseTime" : 2.2, "breach" : 2.2}
  	        	                
  	        	                 ];
	 
	 		
	 
   $rootScope.availabilityData = [
	        	                { "TPageName" : "Customer welcome page after login","NoOfRun" : 14, "AvgRT" : 100, "NoOfErrors" : 0},
	        	                { "TPageName" : "Policy List Page Post Login","NoOfRun" : 14, "AvgRT" : 100, "NoOfErrors" : 0},
	        	                { "TPageName" : "Mode of Payment Page LA MQ Used Post Login","NoOfRun" : 13, "AvgRT" : 95.5, "NoOfErrors" : 1},
	        	                { "TPageName" : "Split Payment information Page LA MQ Used","NoOfRun" : 13, "AvgRT" : 95.5, "NoOfErrors" : 0}
	        	                
	        	                 ];
	        	 
	        	 
	        	
	        	 
	        	 $scope.myDataSource = {
	        			    "chart": {
	        			        "yaxisname": "Sales",
	        			        "bgcolor": "FFFFFF",
	        			        "useroundedges": "1",
	        			        "showPercentValues": "1",
	        			        "showborder": "0",
	        			        "plottooltext": "$label	: $$value",
	        			        "xAxisname": "Pages",
	        	                "yAxisName": "%",
	        			    },
	        			    "data": [
	        			        {
	        			            "label": "Home Page",
	        			            "value": "50%",
	        			            "color": "#F98020"
	        			        },
	        			        {
	        			            "label": "Customer Welcome Page",
	        			            "value": "30%",
	        			            "color": "#DB4939"
	        			        },
	        			        {
	        			            "label": "Policy List Page",
	        			            "value": "70%",
	        			             "color": "#97BB2E"
	        			        },
	        			        {
	        			            "label": "Mode of Payment",
	        			            "value": "80%",
	        			            "color": "#97BB2E"
	        			        }
	        			    ]
	        			}
	        	 
	        	 
	        	   $scope.startTime = DateService.startDate(24)
	        	  //  console.log( "startTime========="+$scope.startTime);
	        	 $scope.startDate = DateService.epochToJsDate($scope.startTime);
	        	// console.log( "startDate========="+$scope.startDate);
	        	  
	        	    $scope.endTime = DateService.endDate()
	        	  //  console.log( "endTime========="+$scope.endTime);
	        	    $scope.endDate = DateService.epochToJsDate($scope.endTime);
	        	//	 console.log( "endDate========="+$scope.endDate);
	        		 //#F4E0EF,
	        		 
	        		$scope.performanceDataSource  = {
	        			    "chart": {
	        			        
	        			        "captionFontSize": "14",
	        			        "subcaptionFontSize": "14",
	        			        "subcaptionFontBold": "0",
	        			        "paletteColors": "#FB24EB,#20FFFF,#0075c2,#1aaf5d",
	        			        "bgcolor": "#ffffff",
	        			        "showBorder": "0",
	        			        "showShadow": "0",
	        			        "showCanvasBorder": "0",
	        			        "usePlotGradientColor": "0",
	        			        "legendBorderAlpha": "0",
	        			        "legendShadow": "0",
	        			        "showAxisLines": "0",
	        			        "showAlternateHGridColor": "0",
	        			        "divlineThickness": "1",
	        			        "divLineIsDashed": "1",
	        			        "divLineDashLen": "1",
	        			        "divLineGapLen": "1",
	        			        "xAxisName": "Day",
	        			        "showValues": "0",
	        			        "showLegend": "1",
	        	                "legendPosition": "RIGHT"
	        			    },
	        			    "categories": [
	        			        {
	        			            "category": [
	        			                {
	        			                    "label": "00"
	        			                },
	        			                {
	        			                    "label": "01"
	        			                },
	        			                {
	        			                    "label": "02"
	        			                },
	        			                {
	        			                    "label": "03"
	        			                },
	        			                {
	        			                    "label": "04"
	        			                },
	        			                {
	        			                    "label": "05"
	        			                },
	        			                {
	        			                    "label": "06"
	        			                },
	        			                {
	        			                    "label": "07"
	        			                }
	        			            ]
	        			        }
	        			    ],
	        			    "dataset": [
	        			        {
	        			            "seriesname": "Home Page Login Page ",
	        			            "data": [
	        			                {
	        			                    "value": "30"
	        			                },
	        			                {
	        			                    "value": "35"
	        			                },
	        			                {
	        			                    "value": "30"
	        			                },
	        			                {
	        			                    "value": "33"
	        			                },
	        			                {
	        			                    "value": "25"
	        			                },
	        			                {
	        			                    "value": "30"
	        			                },
	        			                {
	        			                    "value": "25"
	        			                }
	        			            ]
	        			        },
	        			        {
	        			            "seriesname": "Customer Welcome Page After Login",
	        			            "data": [
	        			                {
	        			                    "value": "15"
	        			                },
	        			                {
	        			                    "value": "17"
	        			                },
	        			                {
	        			                    "value": "14"
	        			                },
	        			                {
	        			                    "value": "20"
	        			                },
	        			                {
	        			                    "value": "10"
	        			                },
	        			                {
	        			                    "value": "05"
	        			                },
	        			                {
	        			                    "value": "03"
	        			                }
	        			            ]
	        			        },
	        			        {
	        			            "seriesname": "Policy List Page Post Login",
	        			            "data": [ 
	        			                {
	        			                    "value": "10"
	        			                },
	        			                {
	        			                    "value": "15"
	        			                },
	        			                {
	        			                    "value": "25"
	        			                },
	        			                {
	        			                    "value": "30"
	        			                },
	        			                {
	        			                    "value": "32"
	        			                },
	        			                {
	        			                    "value": "20"
	        			                },
	        			                {
	        			                    "value": "10"
	        			                }
	        			            ]
	        			        },
	        			        {
	        			            "seriesname": "Mode of Payment Page LA MQ Used",
	        			            "data": [
	        			                {
	        			                    "value": "5"
	        			                },
	        			                {
	        			                    "value": "8"
	        			                },
	        			                {
	        			                    "value": "5"
	        			                },
	        			                {
	        			                    "value": "3"
	        			                },
	        			                {
	        			                    "value": "1"
	        			                },
	        			                {
	        			                    "value": "8"
	        			                },
	        			                {
	        			                    "value": "5"
	        			                }
	        			            ]
	        			        }
	        			    ]
	        			}
	        		
	        		
	        		 //Chart initigration for LINE chart
	        		$scope.performanceChartConfig = {
	        			options: {
	        				chart: { type: 'line',marginTop: 20 },
	        				legend: { enabled:false  },
	        			},
	        			xAxis: {
	        				type:'datetime',
	        				tickWidth: 0,
	        				//tickInterval: 24 * 60 * 3600 * 1000, // one day
	        				 tickInterval: 3600 * 1000,
	        		         minTickInterval: 3600 * 1000,
	        				lineColor: '#000',
	        				lineWidth: 1,


	        			},
	        			yAxis: {
	        				lineColor: '#000',
	        				lineWidth: 1,
	        				gridLineWidth: 0,
	        				title: {
	        					align: 'high',
	        					offset: 0,
	        					rotation: 0,
	        					y:  -10,
	        					text: 'Freq'
	        				},
	        				crosshair: {
	        					width: 1.5,
	        					color: '#89c657',
	        					dashStyle: 'ShortDash'
	        				}
	        			},
	        			series: [{
	        				data: [0, 0, 0, 0, 0,0, 0, 0, 0, 0],
	        				//pointStart: Date.UTC(2010, 0, 1),
	        				//pointInterval: 24 * 3600 * 1000, // one day
	        				color:'#136b82',
	        				marker: { lineWidth: 2 }

	        			}],
	        			title: {
	        				text: ''
	        			},

	        			func: function (chart) {
	        				$scope.$evalAsync(function () {
	        					chart.reflow();
	        					//The below is an event that will trigger all instances of charts to reflow
	        					//$scope.$broadcast('highchartsng.reflow');
	        				});
	        			},

	        			loading: false
	        		}
	        		
	        		//$scope.performanceChartConfig.series[0].data = [] ;
	        		
	        		$scope.performanceChartConfig.series[0].data =  [10, 15, 12, 8, 7,10, 15, 12, 8, 7];

	        		$scope.performanceChartConfig.series[0]  = {
	        			regression: true ,
	        			regressionSettings: {
	        				type: 'linear',
	        				color:  'rgba(223, 83, 83, .9)'
	        			},
	        			data: $scope.performanceChartConfig.series[0].data,
	        			//pointStart: Date.UTC(2010, 0, 1),
	        			//pointInterval: 24 * 3600 * 1000, // one day
	        			color:'#136b82',
	        			marker: { lineWidth: 2 }
	        		}
	        		
	        		
	        		 //Chart initigration for LINE chart
	        		$scope.availabilityColumnChartConfig = {
	        				
	        					        options: {
	        					            chart: {
	        					                type: 'column'
	        					            }
	        					        },
	        					        series: [{
	        					            data: [10, 15, 12, 8, 7]
	        					        }],
	        					        title: {
	        					            text: 'Hello'
	        					        },
	        					        loading: false
	        					   
	        				
	        			    };
	        				
	        	
	        	 
	        		*/
	        	

	             

	        }]);