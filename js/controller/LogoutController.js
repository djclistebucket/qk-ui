'use strict';

/* Logout from cxguardian */

app.controller('LogoutCtrl',  function ( $scope, $rootScope) {
	  console.log("LogoutCtrl reporting for duty.");
	  $rootScope.loggedIn = false;
	  $rootScope.errorMessage ="";
	  localStorage.setItem('loggedIn', false);
	  localStorage.setItem('userName', "");
	  localStorage.setItem('emailId', "");
	  localStorage.setItem('appsList', "");
	  localStorage.setItem('appMetaData', "");
	  localStorage.setItem('transArray', "");
	  localStorage.setItem('treeArray', "");
	  localStorage.setItem('applicationName', "");
	  localStorage.setItem('appLocMapId', "");
	  localStorage.setItem('state', "");
	  localStorage.setItem('clientName', "");
	});