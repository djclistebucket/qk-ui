'use strict';

/* Add application page  */ 


app.controller('AddApplicationCtrl',  function ($scope, $rootScope) {
	  console.log("AddApplicationCtrl reporting for duty.");

	//job title dropdown
	$scope.jobtitleAction = [
		{id: 'title1', name: 'Manager'},
		{id: 'title2', name: 'Director'},
		{id: 'title3', name: 'Asst Director'}
	];
	$scope.selectedJobTitle = $scope.jobtitleAction[0];
	$scope.setJobTitle = function(titles) {
		$scope.selectedJobTitle = titles;
	};

	//days dropdown
	$scope.selectDayAction = [
		{id: 'day1', day: '1'},
		{id: 'day2', day: '2'},
		{id: 'day3', day: '3'}
	];
	$scope.selectedDay = $scope.selectDayAction[0];
	$scope.setDay = function(days) {
		$scope.selectedDay = days;
	};


	$scope.websiteCheckModel = {
		http: false,
		https: false
	};

	$scope.browserCheckModel = {
		firefox: false,
		chrome: false,
		iexplorer: false
	};

	$scope.freqCheckModel = {
		min5: false,
		min15: false,
		min30: false
	};




	 
	});