'use strict';

app.controller('TreeController', ['$scope', '$rootScope','RestApiFactory','DateService','TimeSeriesService',  function ($scope,$rootScope,RestApiFactory,DateService,TimeSeriesService) {

	 console.log("TreeController reporting for duty.");
	 
	 $rootScope.transcationNamesSelected = [];
	 
	 $rootScope.PageNamesSelected = [];
	 
	 $rootScope.lastTransactionSelected = "";
	 
	
		 
	 
	 $rootScope.transcationNamesSelected = TimeSeriesService.getTransactionName();
	 console.log("transcationNamesSelected==="+ JSON.stringify($rootScope.transcationNamesSelected));
	 
	 $rootScope.transcationNamesSelectedLength = $rootScope.transcationNamesSelected.length;
	 
	 $scope.selectedTransactionLength = function(){
		 
		 var length = $rootScope.transcationNamesSelected.length;
		 console.log("length==="+ length);
		 return length;
	 }
	 
	
	 $scope.allTransaction = function(allTranscactionChecked){
		 console.log("I am in allTransaction");
		 console.log("allTranscactionChecked==="+ allTranscactionChecked);
		 $rootScope.stateSelected = "Transactions";
		 console.log("stateSelected==="+ $rootScope.stateSelected);
		 if(allTranscactionChecked){
			 $rootScope.transcationNamesSelected = TimeSeriesService.getTransactionName();
			 console.log("transcationNamesSelected==="+ JSON.stringify($rootScope.transcationNamesSelected));
			 
		 $scope.getTransactionlevelData(allTranscactionChecked,$rootScope.transcationNamesSelected);
		 }else{
			 $rootScope.transcationNamesSelected = [];
			 $rootScope.performanceData  = [];
    		 $rootScope.performanceChartConfig.series = [];
    		 $rootScope.availableChartConfig.series = [];
    		 var seriesBarObj = [];
    		  $rootScope.availabilityColumnChartConfig.series = seriesBarObj;
    		 $rootScope.availabilityData = [];
    		 $rootScope.allTransactionsArray = [];
    		 
			
		 }
		 
	 }
	 
	// Indraneel , Called when you selectd a page node
	 $scope.selectedPages = function(node,nodeCheckedValue){
		 console.log("I am in selectedPages"); 
		 console.log("PageNamesSelected==="+ JSON.stringify($rootScope.PageNamesSelected));
		 
		 $rootScope.stateSelected = "Pages";
		 console.log("stateSelected==="+ $rootScope.stateSelected);
		 if(nodeCheckedValue){
			$rootScope.PageNamesSelected.push(node.name);
		 
		 }else{
			 console.log("remove page name"); 
			 for(var i = $rootScope.PageNamesSelected.length; i--;) {
		          if($rootScope.PageNamesSelected[i] === node.name) {
		        	  $rootScope.PageNamesSelected.splice(i, 1);
		        	  
		          }
		      }
		 }
		 
		 console.log("PageNamesSelected==="+ JSON.stringify($rootScope.PageNamesSelected));
		 console.log("transcationNamesSelected==="+ JSON.stringify($rootScope.transcationNamesSelected));
		 var length = $rootScope.transcationNamesSelected.length;
		 if(length == 1) {
		 var transcationName = $rootScope.transcationNamesSelected.toString();
		 $scope.getPagelevelData(transcationName,$rootScope.PageNamesSelected);
		}else{
			 alert("Can not select another transaction Page");	
		}
		 
	 }
	 
	 // Indraneel , Called when you selectd a txn node
	 $scope.selectedTransaction = function(node,nodeCheckedValue){
		 
		console.log("transcationNamesSelected==="+ JSON.stringify($rootScope.transcationNamesSelected));
		
		//Setting last transaction selected name
		$rootScope.lastTransactionSelected =  node.name;
		console.log("lastTransactionSelected==="+ $rootScope.lastTransactionSelected);
		$rootScope.stateSelected = "Transactions";
		 console.log("stateSelected==="+ $rootScope.stateSelected);
		 
		 if(nodeCheckedValue){
			 $rootScope.transcationNamesSelected.push(node.name);
		 
		 }else{
			 console.log("remove transcation name"); 
			 
			   var length = $rootScope.transcationNamesSelected.length;
			   console.log("length==="+ length);
			   
			   if(length  == 1){
				   alert("At least one transcation should be selected.")
				   console.log("node name"+node.name);
				 				   
			   }else{
			 
			      for(var i = $rootScope.transcationNamesSelected.length; i--;) {
			          if($rootScope.transcationNamesSelected[i] === node.name) {
			        	  $rootScope.transcationNamesSelected.splice(i, 1);
			        	  
			          }
			      }
			
			   }
		 }
		 console.log("transcationNamesSelected==="+ JSON.stringify($rootScope.transcationNamesSelected));
		
		var allTranscactionChecked = false;
		 $scope.getTransactionlevelData(allTranscactionChecked,$rootScope.transcationNamesSelected);
		
	 }
	
	// Indraneel , Called when you selectd a page node and REST API is called. update the right side data
	 $scope.getPagelevelData = function(transcationName,pages){
	     	console.log("I am in getPagelevelData==");
	     	console.log("transcationName"+transcationName);
	     	
	    	console.log("pages==="+ JSON.stringify(pages));
	    	
	    	 var pageNames = [];
	    	 
	    	 pageNames = pages;
	    	 
	    	
	     	 var applicationName = localStorage.getItem('applicationName');
	     	 console.log("applicationName==="+applicationName);
	     	
	     	var clientName = localStorage.getItem('clientName');
	 		console.log("clientName==="+ clientName);
	 		 
	 	 	var urlApp = "/rest/kpiservice/state/availability/"+clientName+"/"+applicationName +"/"+transcationName;
	 		console.log("urlApp==="+ urlApp);
	 		
	 		 $scope.startTime = DateService.startDate(24)
	    	    console.log( "startTime========="+$scope.startTime);
	    	  
	    	    $scope.endTime = DateService.endDate()
	    	    console.log( "endTime========="+$scope.endTime);
	    	    
	 	 
	 	    var post24HrData = {};
	 	    post24HrData.locationName = "Mumbai-QK";
	 	    post24HrData.bandwidth = "524288";
	 		post24HrData.startTime =  $scope.startTime;
	 	    post24HrData.endTime =  $scope.endTime ;
	 	    post24HrData.urlApp = urlApp;
	 	    post24HrData.pageNames = pageNames;
	 	    
	 	// Retrieve availability details data 
	 	   RestApiFactory.getPageDrillData(post24HrData).then(function(data) {
				 console.log("getPageDrillData==="+JSON.stringify(data));
				 var availabilityDetailsTable = [];
		 			
				 availabilityDetailsTable = angular.fromJson(data.response.availabilityDetailsTable);
				 console.log("availabilityDetailsTable=="+ JSON.stringify(availabilityDetailsTable));
				 $rootScope.availabilityData = availabilityDetailsTable;
			
				// Retrieve Page availability bar chart data 
				var type = "Pages"
		 		var seriesBarObj = TimeSeriesService.BarData(data,type);
		    	console.log("seriesBarObj=="+ JSON.stringify(seriesBarObj));
	 		    $rootScope.availabilityColumnChartConfig.series[0] = seriesBarObj;
	    	    console.log("seriesBarObj=="+ JSON.stringify($rootScope.availabilityColumnChartConfig.series[0]));  

				 
		  });
	 	   
	 	// Retrieve time series data for availability
			 var urlApp = "/rest/kpiservice/timeseries/availability/"+clientName+"/"+applicationName + "/"+transcationName;
			 console.log("urlApp==="+ urlApp);
			 post24HrData.urlApp = urlApp;
			 
			  RestApiFactory.getPageDrillData(post24HrData).then(function(data) {
					 console.log(JSON.stringify(data));
					 console.log(JSON.stringify(data.response.multiTimeSeries));
					 
					 $rootScope.availabilityDataSource = TimeSeriesService.timeSeries(data);
					 console.log("availabilityDataSource==="+  $rootScope.availabilityDataSource);
					 $rootScope.availableChartConfig.series =   $rootScope.availabilityDataSource
					 console.log("$scope.availableChartConfig.series==="+  JSON.stringify($rootScope.availableChartConfig.series));
					 
					 
			  });
			  
				// Retrieve time series data for performance
			  
			  var urlApp = "/rest/kpiservice/timeseries/performance/"+clientName+"/"+applicationName + "/"+transcationName;
				 console.log("urlApp==="+ urlApp);
				  
				    post24HrData.urlApp = urlApp;
				  
				   
				    RestApiFactory.getPageDrillData(post24HrData).then(function(data) {
						 console.log(JSON.stringify(data));
						 console.log(JSON.stringify(data.response.multiTimeSeries));
						 
						 $rootScope.performanceDataSource = TimeSeriesService.timeSeries(data);
						 console.log("performanceDataSource==="+  $rootScope.performanceDataSource);
						 $rootScope.performanceChartConfig.series =   $rootScope.performanceDataSource
						 console.log("$scope.performanceChartConfig.series==="+  JSON.stringify($scope.performanceChartConfig.series));
						 
						 
				  });
				    
				    var urlApp = "/rest/kpiservice/state/performance/"+clientName+"/"+applicationName + "/"+transcationName;
					console.log("urlApp==="+ urlApp);
					post24HrData.urlApp = urlApp;  
			
			 // Retrieve performance details data 
			 	   RestApiFactory.getPageDrillData(post24HrData).then(function(data) {
						 console.log("getPageDrillData==="+JSON.stringify(data));
						 var performanceDetailsTable = [];
				 			
						 performanceDetailsTable = angular.fromJson(data.response.performanceDetailsTable);
						 console.log("performanceDetailsTable=="+ JSON.stringify(performanceDetailsTable));
						 $rootScope.performanceData  = performanceDetailsTable;
			 	  });
			  
	 	    
	 	    
	     	
	     }
	 
		// Indraneel , Called when you selected a txn node and REST API is called.update the right side data
	 $scope.getTransactionlevelData = function(allTranscactionChecked,transcationNamesSelected){
	     	console.log("I am in getTransactionlevelData");
	     	console.log("allTranscactionChecked==="+allTranscactionChecked);
	     	$rootScope.stateSelected = "Transactions";
	     	
	     	console.log("PageNamesSelected==="+$rootScope.PageNamesSelected);
	     	
	     	var pageLength = $rootScope.PageNamesSelected.length;
	     	console.log("pageLength==="+pageLength);
	     	if( pageLength > 0 ) {
	     		
	     		   var treeArray = [];
	     		   treeArray =  JSON.parse(localStorage.getItem('treeArray', treeArray));
	     		   console.log("treeArray=="+ JSON.stringify(treeArray));
	     		   
	     		   var transactions = []
	     		   
	     		  transactions =   transcationNamesSelected
	     		  console.log("transactions=="+ JSON.stringify(transactions));
	     		   
	     		 	     		   
	     		   angular.forEach(treeArray, function(transctionItem){
	    				
	     	  			var alltranItem = transctionItem;	
	     	  			console.log("transactionId=== "+alltranItem.id );	
	     	  			console.log("transactionName=== "+alltranItem.name );
	     	  			var allChildren = []; 
	     	  			allChildren =  alltranItem.children;
	     	  			console.log("allChildren=="+ JSON.stringify(allChildren));
	     	  			alltranItem.checked = false;
	     	  			
	     	  			angular.forEach(allChildren, function(childTransactionItem){
	     	  				
	     	  				var tranItem = childTransactionItem;	
	     	  			    console.log("tranItem=="+ JSON.stringify(tranItem));
	     	  			    
	     	  			    var transactionFound = transactions.indexOf(tranItem.name);
	     	  			    console.log("transactionFound=== "+transactionFound);
	     	  			    
	     	  			    if( transactionFound != -1) {
	     	  			    	
	     	  			    	tranItem.checked = true;
	     	  			    	
	     	  			    } else {
	     	  			    	
	     	  			    	tranItem.checked = false;
	     	  			    	
	     	  			    }
	     	  			    
	     	  				//console.log("transactionId=== "+tranItem.id );	
		     	  			//console.log("transactionName=== "+tranItem.name );
		     	  			//console.log("transactionName=== "+tranItem.name );
		     	  			
		     	  		 /*angular.forEach(transactions, function(transactionsSelected){
			     			  
			     			  var transactionNameSelected = transactionsSelected;
			     			 console.log("transactionNameSelected=="+ JSON.stringify(transactionNameSelected));

		     	  			
		     	  			if(tranItem.name == transactionNameSelected ){
		     	  				console.log("transactionName found === "+tranItem.name );	
		     	  				tranItem.checked = true;
		     	  				 var pages = [];
		     	  				pages =  $rootScope.PageNamesSelected;
		     	  			   console.log("pages=="+ JSON.stringify(pages));
		     	  			 angular.forEach(pages, function(pageSelected){
		     	  				var pageName = pageSelected;
		   	     			 console.log("pageName=="+ JSON.stringify(pageName));	 
		     	  			var transactionChildren = []; 
		     	  			transactionChildren =  tranItem.children;
		     	  			
		     	  			angular.forEach(transactionChildren, function(pageItem){
		     	  				
		     	  				var page = pageItem;
		     	  				
		     	  				console.log("pageId=== "+page.id );	
			     	  			console.log("PageName=== "+page.name );
			     	  			
			     	  			if(pageName == page.name) {
			     	  				console.log("pageName found === "+page.name );	
			     	  				page.checked = false;
			     	  			}
			     	  			});
			     	  			
		     	  			});
		     	  			} else {
		     	  				
		     	  				tranItem.checked = false;
		     	  			}
		     	  			
	     	  				
	     	  			});*/
	     		   });
	     	  			
	     		  });
	     		   
	     		  console.log("treeArray=="+ JSON.stringify(treeArray));
	     		 $rootScope.tree  = treeArray;
	     		$rootScope.PageNamesSelected = [];
	     		  
	     		
	     	}
	     
	     	
	     	
	     	
	     	var transcationNamesSelectedLength = transcationNamesSelected.length;
	     	console.log("transcationNamesSelectedLength==="+transcationNamesSelectedLength);
	     	
	    	if(transcationNamesSelectedLength > 0){
	    
	        
	        var transNames = [];
	        transNames = transcationNamesSelected;
	        
	    
	 	 var applicationName = localStorage.getItem('applicationName');
	     	 console.log("applicationName==="+applicationName);
	     	
	     	var clientName = localStorage.getItem('clientName');
	 		console.log("clientName==="+ clientName);
	 		 
	 	 	var urlApp = "/rest/kpiservice/state/availability/"+clientName+"/"+applicationName;
	 		console.log("urlApp==="+ urlApp);
	 		
	 		 $scope.startTime = DateService.startDate(24)
	    	    console.log( "startTime========="+$scope.startTime);
	    	  
	    	    $scope.endTime = DateService.endDate()
	    	    console.log( "endTime========="+$scope.endTime);
	    	    
	 	 
	 	    var post24HrData = {};
	 	    post24HrData.locationName = "Mumbai-QK";
	 	    post24HrData.bandwidth = "524288";
	 		post24HrData.startTime =  $scope.startTime;
	 	    post24HrData.endTime =  $scope.endTime ;
	 	    post24HrData.urlApp = urlApp;
	 	   post24HrData.transactionNames = transNames;
	 	    
	 	// Retrieve availability details data 
	 	   RestApiFactory.getApplicationDrillData(post24HrData).then(function(data) {
				 console.log("getApplicationDrillData==="+JSON.stringify(data));
				 var availabilityDetailsTable = [];
		 			
				 availabilityDetailsTable = angular.fromJson(data.response.availabilityDetailsTable);
				 console.log("availabilityDetailsTable=="+ JSON.stringify(availabilityDetailsTable));
				 $rootScope.availabilityData = availabilityDetailsTable;
			
				// Retrieve Page availability bar chart data 
				var type = "Transactions"
		 		var seriesBarObj = TimeSeriesService.BarData(data,type);
		    	console.log("seriesBarObj=="+ JSON.stringify(seriesBarObj));
	 		    $rootScope.availabilityColumnChartConfig.series[0] = seriesBarObj;
	    	    console.log("seriesBarObj=="+ JSON.stringify($rootScope.availabilityColumnChartConfig.series[0]));  

				 
		  });
	 	   
	 	// Retrieve time series data for availability
			 var urlApp = "/rest/kpiservice/timeseries/availability/"+clientName+"/"+applicationName ;
			 console.log("urlApp==="+ urlApp);
			 post24HrData.urlApp = urlApp;
			 
			  RestApiFactory.getApplicationDrillData(post24HrData).then(function(data) {
					 console.log(JSON.stringify(data));
					 console.log(JSON.stringify(data.response.multiTimeSeries));
					 
					 $rootScope.availabilityDataSource = TimeSeriesService.timeSeries(data);
					 console.log("availabilityDataSource==="+  $rootScope.availabilityDataSource);
					 $rootScope.availableChartConfig.series =   $rootScope.availabilityDataSource
					 console.log("$scope.availableChartConfig.series==="+  JSON.stringify($rootScope.availableChartConfig.series));
					 
					 
			  });
			  
				// Retrieve time series data for performance
			  
			  var urlApp = "/rest/kpiservice/timeseries/performance/"+clientName+"/"+applicationName ;
				 console.log("urlApp==="+ urlApp);
				  
				    post24HrData.urlApp = urlApp;
				  
				   
				    RestApiFactory.getApplicationDrillData(post24HrData).then(function(data) {
						 console.log(JSON.stringify(data));
						 console.log(JSON.stringify(data.response.multiTimeSeries));
						 
						 $rootScope.performanceDataSource = TimeSeriesService.timeSeries(data);
						 console.log("performanceDataSource==="+  $rootScope.performanceDataSource);
						 $rootScope.performanceChartConfig.series =   $rootScope.performanceDataSource
						 console.log("$scope.performanceChartConfig.series==="+  JSON.stringify($scope.performanceChartConfig.series));
						 
						 
				  });
				    
				    var urlApp = "/rest/kpiservice/state/performance/"+clientName+"/"+applicationName;
					console.log("urlApp==="+ urlApp);
					post24HrData.urlApp = urlApp;  
			
			 // Retrieve performance details data 
			 	   RestApiFactory.getApplicationDrillData(post24HrData).then(function(data) {
						 console.log("getPageDrillData==="+JSON.stringify(data));
						 var performanceDetailsTable = [];
				 			
						 performanceDetailsTable = angular.fromJson(data.response.performanceDetailsTable);
						 console.log("performanceDetailsTable=="+ JSON.stringify(performanceDetailsTable));
						 $rootScope.performanceData  = performanceDetailsTable;
			 	  });
			  

	    	}else{
	    		
	    		 $rootScope.performanceData  = [];
	    		 $rootScope.performanceChartConfig.series = [];
	    		 $rootScope.availableChartConfig.series = [];
	    		 var seriesBarObj = [];
	    		  $rootScope.availabilityColumnChartConfig.series = seriesBarObj;
	    		 $rootScope.availabilityData = [];
	    		 $rootScope.allTransactionsArray = [];
	    		 console.log("allTransactionsArray=== "+$rootScope.allTransactionsArray );	
	    		 
	    	}
	     	
	     }
	 
	
		 
	 
	 //Indraneel, junk code
	  var tc = this;
	  
	  // Indraneel. building tree
      buildTree();
      function buildTree() {
    	 
	 
    	  $rootScope.tree = [];
    	
    
    	  $scope.performanceData = [];
    	  
    	     var appParam = {};
    	     var appLocMapId = localStorage.getItem('appLocMapId');
    	     console.log("appLocMapId==="+ appLocMapId);
    	     var clientName = localStorage.getItem('clientName');
    	     console.log("clientName==="+ clientName);
    	     var urlApp = "/rest/kpiservice/metadata/"+clientName+"/"+appLocMapId;
    	     appParam.urlApp = urlApp;
    	     console.log("urlApp==="+ appParam.urlApp);
    	     
    	   
    	     
    	     var promise1 = RestApiFactory.getDrilldownMenu(appParam).then(function(data)
 	       		    {
 	       		
 			       		if(data.statusCode == 1)
 				    	   {
 				    	 console.log("statusCode=== 1" );
 				    	 $scope.availability = "0";
 				    	   
 				    	   } else{
 				    		  console.log("statusCode=== 0" );
 				    		 console.log("data=== "+  JSON.stringify(data.response.applications) );
 				    		 
 				    		 var applicationArray = [];
 				    		 
 				    		applicationArray = angular.fromJson(data.response.applications);
 				    		 console.log("applicationArray=== "+ applicationArray );
 				    		 
 				    		var treeArray = [];
 				    		
 				    	
 				    		
 				    		//treeArray.push(allTransactionData);
 				    		 
 				    		angular.forEach(applicationArray, function(applicationItem){
 				    			
 				    			var appItem = applicationItem;
 				    			console.log("appLocMapId=== "+appItem.appLocMapId );
 				    			console.log("applicationId=== "+appItem.applicationId );
 				    			console.log("applicationName=== "+appItem.applicationName );
 				    			
 				    			var transArray = [];
 				    			var transactionData = {};
 				    			var transactionArray = [];
 				    			
 				    			
 				    			transArray = angular.fromJson(appItem.transactions);
 				    			console.log("transArray============ "+transArray);
 				    			localStorage.setItem('transArray',JSON.stringify(transArray));
 				    			
 				    			var allTranChildArray = [];
 				    			
 				    			angular.forEach(transArray, function(transctionItem){
 				    				
 				    			var tranItem = transctionItem;	
 				    			console.log("transactionId=== "+tranItem.transactionId );	
 				    			console.log("transactionName=== "+tranItem.transactionName );
 				    			
 				    			 var transactionData = {
 				    					"id": "transcation_"+tranItem.transactionId,
 								 	    "name":tranItem.transactionName,
 								 	    "checked": true
 							 		    	 	  }
 				    			
 				    			
 				    			
 				    			var pageArray = [];
 				    			
 				    			pageArray = angular.fromJson(tranItem.pages);
 				    			
 				    			if(pageArray.length > 0) {
 				    				
 				    				var pageChildArray = [];
 				    				
 				    				
 				    			angular.forEach(pageArray, function(pageItem){
 				    			
 				    				var page = pageItem;
 				    				console.log("pageId=== "+page.pageId );
 				    				console.log("pageName=== "+page.pageName );
 				    				
 				    				var pageIdName = {
 				    						"id": "page_"+ page.pageId,		
 				    						"name":page.pageName,
 				    						 "checked": false
 				    				}
 				    				
 				    				pageChildArray.push(pageIdName);
 				    				
 				    				
 				    				
 				    				
 				    			});
 				    			
 				    			 				    			
 				    			
 				    			}
 				    			
 				    			var pageData = {
 				    					"children":pageChildArray			
 				    			}
 				    			
 				    			$.extend(transactionData,pageData);
 				    			
 				    			transactionArray.push(transactionData);
 				    			
 				    			
 				    				
 				    			});
 				    			
 				    			var allTransactionData = {
	 				    				 "name": "All Transactions",
	 				    				 "id" : "all",
	                                     "checked": true,
	                                     "children":transactionArray
	 				    		}
				    			
				    			
				    			treeArray.push(allTransactionData);
 				    			
 				    			 localStorage.setItem('treeArray', JSON.stringify(treeArray));
 				    			
 				    			
 				    		});
 				      		 
 				    		
 				    		
 				    		$rootScope.tree  = treeArray;
 				    		console.log("$scope.tc.tree=== "+JSON.stringify( $scope.tree));
 				    		$rootScope.transactionDisplay = true;
 				    		
 				    		 
 				    		 
 				    	   }
 	       		    });
 	       		    
 	       		   
 	       		    
 	       		 
    	     	 
      } // indraneel end of buildtree


      /*	     
        
                                
        */                    
       
      

	
    

//}
      
      $rootScope.tree = [
                        {
                          "name": "All Transactions",
                          "checked": false
                        }


                      ];

}
      ]);





app.directive('nodeTree', function () {
    return {
        template: '<node ng-repeat="node in tree"></node>',
        replace: true,
        restrict: 'E',
        scope: {
            tree: '=children'
        }
    };
});
app.directive('node', function ($compile,$rootScope) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'partials/node.html', // HTML for a single node.
        link: function (scope, element) {
            /*
             * Here we are checking that if current node has children then compiling/rendering children.
             * */
        	
            if (scope.node && scope.node.children && scope.node.children.length > 0) {
                scope.node.childrenVisibility = true;
                var childNode = $compile('<ul class="tree" ng-if="!node.childrenVisibility"><node-tree children="node.children"></node-tree></ul>')(scope);
                element.append(childNode);
            } else {
                scope.node.childrenVisibility = false;
            }
        },
        controller: ["$scope", function ($scope,$rootScope) {
        	
        	
        	
            // This function is for just toggle the visibility of children
            $scope.toggleVisibility = function (node) {
            	
                if (node.children) {
                
                    node.childrenVisibility = !node.childrenVisibility;
                }
            };
            // Indraneel . checking if node is page or trx.  Here We are marking check/un-check all the nodes.
            $scope.checkNode = function (node) {
            	console.log("node name=="+node.name);
            	console.log("node id=="+node.id);
            	console.log("node checked=="+node.checked);
            	var pages = [];
            
            	var str = node.id;
            	var checkedValue = node.checked;
            	
            	if(str == "all"){
            		 console.log("Invoked All Transacation");
            		 console.log("checkedValue=="+checkedValue);
            		 var allTranscactionChecked = !checkedValue;
            		 console.log("allTranscactionChecked=="+allTranscactionChecked);
            		 $scope.allTransaction(allTranscactionChecked);
            		 checkChildren(node);	 
            	}else if (str.search("transcation") == 0 ){
            	
            		 console.log("Invoked Single Transacation");
            	 	        			   
            		  var nodeCheckedValue = !node.checked;
            		  $scope.selectedTransaction(node,nodeCheckedValue);
            		
            		  //console.log("selectedTransaction=="+JSON.stringify($rootScope.selectedTransaction));
            		  
            		 // checkChildren1(node)
            		 // console.log("pages=="+JSON.stringify(pages));
            		// $scope.getPagelevelData(node,pages);
            	  		
            	  } else if (str.search("page") == 0 ){
            		  console.log("Invoked Single Page");
            		  
            		  var transactionLength =  $scope.selectedTransactionLength();
            		  
            		  console.log("transactionLength=="+transactionLength);
             		 // checkChildren1(node)
            		  if( transactionLength > 1 ){
            			 // checkChildren(node)
            			 console.log("node.checked=="+node.checked);
            			  
            		  }else{
            		  var nodeCheckedValue = !node.checked;
             		  $scope.selectedPages(node,nodeCheckedValue);
            		  }
            		  }
            	
            
            	
            	 
            	
            
                node.checked = !node.checked;
            
                
                function checkChildren(c) {
                	
                    angular.forEach(c.children, function (c) {
                    	console.log("transaction Name ="+c.name);
                    	
                        c.checked = node.checked;
                       // checkChildren(c);
                    });
                }
                
                function checkChildren1(c) {
                	
                    angular.forEach(c.children, function (c) {
                       
                        var name = c.name
                        
                        c.checked = node.checked;
                        console.log("pageName="+name);
                        pages.push(name);
                       // checkChildren1(c);
                    });
                }
                
                function checkChildren2(c,flag) {
                	console.log("=============flag="+flag);
                    angular.forEach(c.children, function (c) {
                       
                        var name = c.name
                        
                        c.checked = true;
                        console.log("pageName="+name);
                        pages.push(name);
                       // checkChildren1(c);
                    });
                }
                
                if(str == "all"){
                checkChildren(node);
                }
               
            };
            
                       
        }]
    };
});



