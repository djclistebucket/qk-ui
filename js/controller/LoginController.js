'use strict';

/* This controller is used for Logging user and if successful redirect to dashboard page if fails redirect to login page */
app.controller('LoginCtrl', ['$scope', '$rootScope','$http', '$location','$q','$window','RestApiFactory', function ($scope,$rootScope, $http, $location, $q,$window,RestApiFactory) {
	
	console.log("Login Controller reporting for duty.");
	
	$rootScope.loggedIn = false;
	  $rootScope.errorMessage ="";
	  localStorage.setItem('loggedIn', false);
	  localStorage.setItem('userName', "");
	  localStorage.setItem('emailId', "");
	  localStorage.setItem('appsList', "");
	  localStorage.setItem('appMetaData', "");
	  localStorage.setItem('transArray', "");
	  localStorage.setItem('treeArray', "");
	  localStorage.setItem('applicationName', "");
	  localStorage.setItem('appLocMapId', "");
	  localStorage.setItem('state', "");
	  localStorage.setItem('clientName', "");
	  
	  
	  $scope.logout = function () {
		  console.log("In logout function");
		  localStorage.setItem('loggedIn', false);
	      $location.path('/login');
	  };
	  
	 console.log("$rootScope.errorMessage=="+$rootScope.errorMessage);
	$scope.user= {};
	
	$scope.errorMessage = $rootScope.errorMessage;
	
	 var userInfo;
	
	 $scope.searchButtonText = "LOG IN";
	 $scope.loading = false;
	
	 
	 $scope.submitForm = function() {
		 
		 //This code is for spinning 
		 $scope.loading = true;
	    $scope.searchButtonText = "Loading";
		 
		
	       
		// console.log("email==="+$scope.user.email);
		// console.log("password==="+$scope.user.password);
		 var urlApp = '/rest/accesscontrol/authenticate/login';
		 var appParam = {};
		 appParam.emailId = $scope.user.email;
		 appParam.password = $scope.user.password;
		 appParam.urlApp = urlApp;
		 
		 
		 var promise =  RestApiFactory.getUserAuthentication(appParam).then(function(data)
	       		    {
	       		     
			    console.log(data);
		    	console.log(data.authenticated);
		    	var auth = data.authenticated;
		    	var clientName = data.clientName;
		    	var user = data.userName;
		    	var phoneNumber = data.phoneNumber;
		    	var emailId = data.emailId;
		    	console.log("auth = "+auth);
		    	console.log("client = "+clientName);
		    	console.log("user = "+user);
		    	console.log("emailId = "+emailId);
		    	
		    	
		    	if(auth){
		    		
		    		//  if(user ="test"){
		    		//	  user = "Swapnil Mehta";
		    		//  }
		    		  $rootScope.userName = user;
		    		  $rootScope.emailId = emailId;
		    		  $rootScope.password = $scope.user.password;
		    		  $rootScope.clientName = clientName;
		    		  $rootScope.loggedIn = true;
		    		  
		    		  
		    		
		    		  
		    		 userInfo = {
		    			        accessToken:auth,
		    			        userName: user,
		    			        clientName: clientName
		    			      };
		    			      $window.sessionStorage["userInfo"] = JSON.stringify(userInfo);
		    			      $scope.loading = false;
		    			      
		    			      
		    			      
		    			      localStorage.setItem('loggedIn', true);
		    			      localStorage.setItem('clientName', $rootScope.clientName);
		    			      localStorage.setItem('userName', $rootScope.userName);
		    			      localStorage.setItem('emailId', $rootScope.emailId);
		    			      
		    		  $location.path( "/" );
		    	}else{
		    		
		    		 $scope.errorMessage = "Invalid username or password";
		    		 console.log($scope.errorMessage);
		    		 $scope.loading = false;
		    		 $scope.searchButtonText = "LOG IN";
		    		$location.path( "/login" );
		    		
		    	}
		    	
	       		   
	       		    });
		 
	
	 
	 }
	}]);