'use strict';

/* This Controller is used to display cxguardian dashboard */

app.controller('DashboardCtrl', ['$scope', '$rootScope', '$window' , '$http' , '$filter','DateService','$q','RestApiFactory','$timeout','$location','CommonUtilService',  function ($scope,$rootScope, $window , $http , $filter,DateService,$q,RestApiFactory,$timeout,$location,CommonUtilService) {

	 console.log("DashboardCtrl reporting for duty.");
	 
	 /*Indraneel Functional will be invoked when user clicks on Dashboard application row to drill down further.
	 * Initialize the tree
	 */
	 $scope.drilldown = function(applicationName,state) {
        console.log("drilldown function invoked");
        console.log("applicationName==="+applicationName);
        console.log("state==="+state);
        localStorage.setItem('applicationName', applicationName);
        var appMetaDataArray = [];
		 
		 appMetaDataArray = JSON.parse(localStorage.getItem('appMetaData'));
		 console.log("appMetaData==="+ appMetaDataArray.length);
		 
		/*
		 * Setting applicationName and applocmapid for the selected application
		 */ 
        CommonUtilService.setAppData(appMetaDataArray,applicationName);
        
        var appParam = {};
	     var appLocMapId = localStorage.getItem('appLocMapId');
	     console.log("appLocMapId==="+ appLocMapId);
	     var clientName = localStorage.getItem('clientName');
	     console.log("clientName==="+ clientName);
	     var urlApp = "/rest/kpiservice/metadata/"+clientName+"/"+appLocMapId;
	     appParam.urlApp = urlApp;
	     console.log("urlApp==="+ appParam.urlApp);
	     
	     //Indraneel Calling rest api to get the tree values for txn.
	     var promiseDrill = RestApiFactory.getDrilldownMenu(appParam).then(function(data)
       		    {
       		
		       		if(data.statusCode == 1)
			    	   {
			    	 console.log("statusCode=== 1" );
			    	  $scope.availability = "0";
			    	   var treeArray = [];
			    	   treeArray =  localStorage.setItem('treeArray', treeArray);
			    	   $rootScope.tree  = treeArray;
			    	   $rootScope.transactionDisplay = false;
			    	   
			    	   } else{
			    		  console.log("statusCode=== 0" );
			    		  // setting treeArray from REST API response
			    		 CommonUtilService.setTransactionData(data);
			    		 treeArray =  localStorage.getItem('treeArray', treeArray);
			    		 $rootScope.tree = treeArray;
			    		 
			    		//Indraneel transactionDisplay is used to control whether api calls need to be made to refresh data on the right side
			    		 $rootScope.transactionDisplay = true;
			    		 
			    		
			    	   }
       		    }); // end of rest call
	     
	     
	     promiseDrill.then(function(){
        
        
        if(state == "a"){
        	state = "Availability";	
        }else{
        	state = "Performance";	
        }
        
        console.log("state==="+state);
        localStorage.setItem('state', state);
        
        //Indraneel redirecting to drill down page
        $location.path("/drilldown");
	     });
     }
	 
	 $scope.myAction = function() {
	        console.log("myAction function invoked");
	         $location.path("/myaction");
	     }
	 
	 
	 var userInfo = $window.sessionStorage["userInfo"];
	 console.log("clientName="+  $rootScope.clientName);
	// console.log("locationName="+  $rootScope.locationName);
	// console.log("bandwidth="+  $rootScope.bandwidth);
	 console.log("password="+  $rootScope.password);
	 console.log("emailId="+  $rootScope.emailId);
	 
	 $scope.getApplication = null;
	 
	 var urlApp = "/rest/kpiservice/metadata/"+ $rootScope.clientName
	 console.log("urlApp="+ urlApp);
	 
	 var appParam = {};
	 appParam.clientName = $rootScope.clientName;
	 appParam.locationName = "Mumbai-QK";
	 appParam.bandwidth = "524288";
	 appParam.emailId = $rootScope.emailId;
	 appParam.password = $rootScope.password;
	 appParam.urlApp = urlApp;
			
		 
		 RestApiFactory.getApplication(appParam).then(function(data)
	 
	    {
			 $scope.applications = [];	 
			 var appsList = [];	
			
	      $scope.getApplication = data.response;
	      console.log( JSON.stringify($scope.getApplication));
	      
	      	//Indraneel  Spinner value initialization. May not be use currently
	         $scope.dataLoaded1Hr = false; 
		  	 $scope.dataLoaded24Hr = false; 
		  	 $scope.dataLoaded = false;
		  	 
		  	 //Indraneel , if not application is registerd route to other page (to be handled yet)
		  	 if($scope.getApplication == undefined) {
		  		 
		  		$rootScope.message = " "
		  	 }
		  	 
		  	 var statusCode = data.statusCode;
		  	 console.log("============= statusCode"+statusCode);
		  	 
		  	 
	     if($scope.getApplication != undefined) {
	    	 
	    	 console.log("=============  !undefined");
	     
	      
	      $scope.applicationList = $scope.getApplication.metadataEntry;
	      
	      //localStorage.setItem('appList', $scope.applicationList);
	      
	     // $scope.appsList = [];	
	      var appMetaData =  $scope.applicationList;
	      localStorage.setItem('appMetaData', JSON.stringify(appMetaData));
	      
	     	angular.forEach($scope.applicationList, function(item){
	     		console.log("item=="+item);	
	     		$scope.apps = item ;
	     		
	     		//Indraneel to be removed later as it may not be in used.
	            $rootScope.appName =  $scope.apps.applicationName;
	            console.log("===applicationName=="+$scope.appName);
	            
	            var applicationName = $scope.apps.applicationName;
	            console.log("===applicationName=="+applicationName);
	            
	            appsList.push($scope.appName);
	            
	            $scope.clientName =  $scope.apps.clientName
	            console.log("===clientName=="+$scope.clientName);
	            
	            $scope.imagePath = "http://storage.googleapis.com/cxguardianprod.appspot.com/client/"+$rootScope.clientName +"/applications/"+ $scope.appName + "/appscreenshot.png"
	            console.log("===imagePath=="+$scope.imagePath);
	            
	            $scope.deviceImagePath = "http://storage.googleapis.com/cxguardianprod.appspot.com/client/"+$rootScope.clientName +"/applications/"+ $scope.appName + "/device.png"
	            console.log("===deviceImagePath=="+$scope.deviceImagePath);
	            
	            
	            $scope.locationName =  $scope.apps.locationName
	            console.log("===locationName=="+$scope.locationName);
	            
	            $scope.bandwidth =  $scope.apps.bandwidth
	            console.log("===bandwidth=="+$scope.bandwidth);
	            
	            
	            var type = "";
	            var description = "";
	            //Indraneel applicationData is initalized . 
	            var applicationData = {
		    	 	    "applicationName": applicationName,
		    	 	     "type": type,
		    	 	     "description": description,
		    	 	     "imagePath": $scope.imagePath,
		    	 	    "locationName": $scope.locationName,
		    	 	    "deviceImagePath":$scope.deviceImagePath
		    	 	   }
	            
	            $scope.types = [];
	            
	            
	            
	            $scope.startTime = DateService.startDate(24)
	      	    console.log( "startTime========="+$scope.startTime);
	      	  
	      	    $scope.endTime = DateService.endDate()
	      	    console.log( "endTime========="+$scope.endTime);
	      	    
	      	    var urlAvailability = "/rest/kpiservice/state/availability/"+ $rootScope.clientName +"/"+applicationName;
		    	console.log("urlAvailability="+ urlAvailability);
		    	
		    	 $scope.startTime1Hr = DateService.startDate(1)
		      	 console.log( "startTime1Hr========="+$scope.startTime1Hr);
	      	    
		    	 //Indraneel Setting input parameters for 1 hour availability data
	      	    var post1HrData = {};
	       	    post1HrData.locationName =  $scope.locationName;
	       	    post1HrData.bandwidth = $scope.bandwidth;
	       	    post1HrData.startTime = $scope.startTime1Hr;
	       	    post1HrData.endTime = $scope.endTime;
	       	    post1HrData.emailId = $rootScope.emailId;
	       	    post1HrData.password = $rootScope.password;
	       	    post1HrData.urlApp = urlAvailability;
	       	    
	       	   	       	
	       	 
	       	 var promise1 = RestApiFactory.getChartData(post1HrData).then(function(data)
	       		    {
	       		
			       		if(data.statusCode == 1)
				    	   {
				    	 console.log("statusCode=== 1" )
				    	 $scope.availability = "0";
				    	   
				    	   } else{
	       		      $scope.getAvailability1HR = data.response;
	       		       console.log( JSON.stringify(data.response));
	       		       
	       		      console.log( JSON.stringify($scope.getAvailability1HR));
	       		      $scope.availability = $scope.getAvailability1HR.state;
	       		    //  console.log("1 hr availability==="+  $scope.availability );
	       		   
	       		      // Getting ready for high chart
	       		      $scope.availabilityChartConfig1Hr.series[0].data[0] =  $scope.availability
	       		      
				    	   }
	       		    
         	    	 var circleData1HR = {
 	   		    	 	    "availabilityChartConfig1Hr": $scope.availabilityChartConfig1Hr
 	   		    	 	  }
        	    	 $.extend(applicationData,circleData1HR);
         	    
	       		    }); // indraneel end of rest call
	       	 
	       	 
	       
	       	promise1.then(function(){

	       	 
	       	    
	       	    var post24HrData = {};
	       	    post24HrData.locationName =  $scope.locationName;
	       	    post24HrData.bandwidth = $scope.bandwidth;
	       	    post24HrData.startTime = $scope.startTime;
	       	    post24HrData.endTime = $scope.endTime;
	       	    post24HrData.emailId = $rootScope.emailId;
	       	    post24HrData.password = $rootScope.password;
	       	    post24HrData.urlApp = urlAvailability;
	       	    
	       	 var promise2 =  RestApiFactory.getChartData(post24HrData).then(function(data)
	       		    {
	       		    
		       		if(data.statusCode == 1)
			    	   {
			    	 console.log("statusCode=== 1" )
			    	$scope.availability24HR = "0";
			    	   
		    	   }else{
	       		 
	       		      $scope.getAvailability24Hr = data.response;
	       		      console.log( JSON.stringify($scope.getAvailability24Hr));
	       		      $scope.availability24HR = $scope.getAvailability24Hr.state;
	       		      console.log("24 hr availability==="+  $scope.availability24HR );
	       		   $scope.availabilityChartConfig24Hr.series[0].data[0] =  $scope.availability24HR;
	       		      
		    	   }
       	    	/* var circleData24HR = {
	   		    	 	    "available24HR": $scope.availability24HR
	   		    	 	  }
	   		    	 	  */
		       		
		       	 var circleData24HR = {
	   		    	 	    "availabilityChartConfig24Hr": $scope.availabilityChartConfig24Hr
	   		    	 	  }
		       	 
       	    	 $.extend(applicationData,circleData24HR);
       	    	 
       	    	 
       	    	
	       		     
	       		    }); // indraneel end of rest call for 24 hrs
	       	 
	       	 //Indraneel Performance avg value diplayed on top of Performance timeseries
	       	promise2.then(function(){
	       		//console.log("applicationName="+  applicationName);
	       		var urlPerformanceAvg = "/rest/kpiservice/state/performance/"+ $rootScope.clientName +"/"+ applicationName;
		    	 console.log("urlPerformance="+ urlPerformanceAvg);
		    	 
		    	    var postPerfAvgData = {};
		    	    postPerfAvgData.locationName =  $scope.locationName;
		    	    postPerfAvgData.bandwidth = $scope.bandwidth;
		    	    postPerfAvgData.startTime = $scope.startTime;
		    	    postPerfAvgData.endTime = $scope.endTime;
		    	    postPerfAvgData.emailId = $rootScope.emailId;
		    	    postPerfAvgData.password = $rootScope.password;
		    	    postPerfAvgData.urlApp = urlPerformanceAvg;
		       	  
		    	    var promise4 =  RestApiFactory.getChartData(postPerfAvgData).then(function(data)
			       		    {
			       		   
		    	    	  if(data.statusCode == 1)
						   {
						 console.log("statusCode=== 1" )
						$scope.avg  = 0;

					   }else {
		    	    	
		    	    	      $scope.getPerfData = data.response;
			       		      console.log( JSON.stringify($scope.getPerfData));
			       		     $scope.avg = $scope.getPerfData.state / 1000;
			       		      console.log("Avg==="+   $scope.avg.toFixed(2));
			       		      
			       		      $rootScope.avgPerformance = $scope.avg.toFixed(2);
			       		   console.log("avgPerformance==="+   $rootScope.avgPerformance);
			       		      
					   }
			       		      
			       		   var timeSeriesAvgPerf = {
		    	   	    	 	    "timeSeriesAvgPerf": $scope.avg.toFixed(2)
		    	   	    	 	  }
			       		      
			       		     $.extend(applicationData,timeSeriesAvgPerf);
			       		   
			       		    });
		    	    
		    	    promise4.then(function(){
		    	    
		    	    	//console.log("applicationName"+  applicationName);
		    	    	var urlPerformanceTimeSeries = "/rest/kpiservice/timeseries/performance/"+ $rootScope.clientName +"/"+ applicationName;
		   	    	   console.log("urlPerformanceTimeSeries="+ urlPerformanceTimeSeries);
		   	    	 
		   	    	 
		   	    	    $scope.startTime = DateService.startDate(24)
		   	      	  //  console.log( "startTime========="+$scope.startTime);
		   	      	  
		   	      	    $scope.endTime = DateService.endDate()
		   	      	 //   console.log( "endTime========="+$scope.endTime);
		   	    	 
		   	    	 
		   	    	    var postPerfData = {};
		   	    	    postPerfData.locationName =  $scope.locationName;
		   	    	    postPerfData.bandwidth = $scope.bandwidth;
		   	    	    postPerfData.startTime = $scope.startTime;
		   	    	    postPerfData.endTime = $scope.endTime;
		   	    	    postPerfData.emailId = $rootScope.emailId;
		   	    	    postPerfData.password = $rootScope.password;
		   	    	    postPerfData.urlApp = urlPerformanceTimeSeries;
		   	       	    
		   	       	 //Indraneel Calling time Series for Performance
		   	    	    var promise3 =  RestApiFactory.getChartData(postPerfData).then(function(data)
		   	       		    {
		   	    	    	
		   	    	     //Chart initigration for LINE chart
		   	    	    	//Indraneel Look and feel of High chart is here ..
		                	var performanceChartConfig = {
		                		options: {
		                			chart: { type: 'line',marginTop: 20 },
		                			legend: { enabled:false  },
		                			global: { useUTC : false }
		                		},
		                		xAxis: {
		                			type:'datetime',
		                			tickWidth: 0,
		                			//tickInterval: 24 * 60 * 3600 * 1000, // one day
		                			 tickInterval: 3600 * 1000,
		                	         minTickInterval: 3600 * 1000,
		                			lineColor: '#000',
		                			lineWidth: 1,
		                /*			title: {
		                				align: 'middle',
		                				offset: -15,
		                				text: 'Sec'
		                			},*/

		                		},
		                		yAxis: {
		                			lineColor: '#000',
		                			lineWidth: 1,
		                			gridLineWidth: 0,
		                			title: {
		                				align: 'high',
		                				offset: 0,
		                				rotation: 0,
		                				y:  -10,
		                				text: 'Secs'
		                			},
		                			crosshair: {
		                				width: 1.5,
		                				color: '#89c657',
		                				dashStyle: 'ShortDash'
		                			}
		                		},
		                		series: [{
		                			data: [0, 0, 0, 0, 0,0, 0, 0, 0, 0],
		                			//pointStart: Date.UTC(2010, 0, 1),
		                			//pointInterval: 24 * 3600 * 1000, // one day
		                			color:'#136b82',
		                			marker: { lineWidth: 2 }

		                		}],
		                		title: {
		                			text: ''
		                		},
		                		
		                		 func: function (chart) {
		                			$scope.$evalAsync(function () {
		                				chart.reflow();
		                				//The below is an event that will trigger all instances of charts to reflow
		                				//$scope.$broadcast('highchartsng.reflow');
		                			});
		                		},

		                		loading: false
		                	} 
		                	
		                	performanceChartConfig.series[0]  = {
		                			regression: true ,
		                			regressionSettings: {
		                				type: 'linear',
		                				color:  'rgba(223, 83, 83, .9)',
		                			},
		                			//data: $scope.performanceChartConfig.series[0].data,
		                			//pointStart: Date.UTC(2010, 0, 1),
		                			//pointInterval: 24 * 3600 * 1000, // one day
		                			color:'#136b82',
		                			marker: { lineWidth: 2 },
		                			
		                		}
		                	
		   	    	    	
		   	       		    
		   	    	     if(data.statusCode == 1)
						   {
						 console.log("statusCode=== 1" )
						$scope.avg  = 0;
						    performanceChartConfig.series[0].data = [0, 0, 0, 0, 0,0, 0, 0, 0, 0] ;
		                   	var performanceChartObj = 
		                   		{
		                   			performanceChartConfig: performanceChartConfig
		                   			
		                   		}
		                   	
		                   	console.log("performanceChartObj:"+JSON.stringify(performanceChartObj));
		                   	
		                    $.extend(applicationData, performanceChartObj);
						

					   }else {
		   	    	    	
		   	    	    	  $scope.getPerfData = data.response;
		   	       		      console.log( JSON.stringify($scope.getPerfData));
		   	       		      
		   	       		    //Indraneel time series values - name , value pair is set here .  
		   	       			$scope.timeSeriesData = $scope.getPerfData.timeseries;
		                   	//console.log("timeSeriesData=="+$scope.timeSeriesData);
		   	       			
		   	       			
		                   	
		                   	
		         			      
		         			      $scope.data = [];
		         			      var timeData = [];
		         			     var data1 = [];
		         			  
		         			      
		                   	
		                   	angular.forEach($scope.timeSeriesData, function(item){
		                   	     console.log("item=="+item.value);
		      		    		 console.log("item=="+item.time);
		                   		 var dataset = [];
		                   		
		      		    		var value = item.value;
		      		    		//Indraneel response time
		      		    		$scope.valueInSeconds = value/1000;
		      		    		console.log("valueInSeconds=="+$scope.valueInSeconds);
		      		    		
		      		    		var time = item.time;
		      		    		//Indraneel epoc time 
		      		    		$scope.dateRange = time;
		      		    		console.log("dateRange=="+$scope.dateRange);
		      		    		
		      		    	///	$scope.date1 = EpochToDate(time);
		      		    		//console.log("date1=="+$scope.date1);
		      		    		
		      		    		// $scope.HoursMins = $filter('date')(new Date($scope.date1),'hh:mm');
		      		    	 	// console.log("HoursMins:"+$scope.HoursMins);
		      		    	 	 
		      		    	 //	var label = "label";
		      		    	 //	var label = $scope.HoursMins;
		      		    	 	var value = JSON.stringify($scope.valueInSeconds);
		      		    	
		      		    	 	dataset.push(time);
		      		    	 	dataset.push($scope.valueInSeconds);
		      		    	 	console.log("dataset:"+JSON.stringify(dataset));
		      		    	 	
		      		    	 //	data1.push($scope.valueInSeconds);
		      		    		 
		      		    	 	data1.push(dataset);
		      		    	  
		      		    	 	
		      		    	 
		      		    		
		      		    	});
		                   	
		                   
		                   	
		                   
		                	
		                	
		                   	
		                	performanceChartConfig.series[0].data = data1 ;
		                	console.log("===============data1:"+JSON.stringify(data1));
		                	//console.log("===============data1:"+JSON.stringify(performanceChartConfig.series[0].data ));
		                	
		                	
		                   	var performanceChartObj = 
		                   		{
		                   			performanceChartConfig: performanceChartConfig
		                   			
		                   		}
		                   	
		                   	console.log("performanceChartObj:"+JSON.stringify(performanceChartObj));
		                   	//Timeseries added.
		                    $.extend(applicationData, performanceChartObj);
		                
					   }	
		          		   
			   	       		      
		   	       		    });
		   	    	    
		   	    	 promise3.then(function(){
		   	    		 
		   	    		
		   	    		var urlErrorData = "/rest/kpiservice/state/error/"+ $rootScope.clientName +"/"+ applicationName;
		   	    		console.log("urlErrorData=="+JSON.stringify(urlErrorData));
		   	    		
		   	    	   
			   	    	//Last 15 minutes
			   	    	var hr = 900/3600;
			   	    	$scope.startTime15Mins = DateService.startDate(hr)
			      	    console.log( "startTime========="+$scope.startTime15Mins);
			   	    	
			   	    	$scope.endTime = DateService.endDate()
			      	    console.log( "endTime========="+$scope.endTime);
			   	    	
			   	    	var postErrorData = {};
		   	    	  	postErrorData.locationName =  $scope.locationName;
			   	    	postErrorData.bandwidth = $scope.bandwidth;
			   	    	postErrorData.emailId = $rootScope.emailId;
			   	    	postErrorData.password = $rootScope.password;
			   	    	postErrorData.urlApp = urlErrorData;
			   	    	postErrorData.startTime = $scope.startTime15Mins;
			   	    	postErrorData.endTime = $scope.endTime;
		   	    	    
		   	    	 var Error15mins =  RestApiFactory.getChartData(postErrorData).then(function(data)
			   	       		    {
				   	    		if(data.statusCode == 1)
						    	   {
						    	 console.log("statusCode=== 1" )
						    	 $scope.getError15Mins = 0;
						    	 console.log("getError15Mins=== "+$scope.getError15Mins  ) ;
						    	 
						    	   } else{
						    		   console.log("statusCode=== 0" ) 
						    		   console.log( JSON.stringify(data.response));
						    		   $scope.getError15Mins = data.response.state;
						    		   console.log("getError15Mins=== "+$scope.getError15Mins ) ;
						    	   }
				   	    		
				   	    		var Error15MinObj = 
		                   		{
		                   			Error15Mins: $scope.getError15Mins
		                   			
		                   		}
		                   	
				   	    		console.log("Error15MinObj:"+JSON.stringify(Error15MinObj));
		                   	
				   	    		$.extend(applicationData, Error15MinObj);
				   	    		
				   	    	
		   	    		 
			   	       		    });
		   	    	 
		   	    	 
		   	    	 	Error15mins.then(function(){
		   	    	 	
		   	    	 	//Last 30 minutes
			   	    	var hr = 1800/3600;
			   	    	$scope.startTime30Mins = DateService.startDate(hr)
			      	    console.log( "startTime========="+$scope.startTime30Mins);	
		   	    	 	
			   	    	postErrorData.startTime = $scope.startTime30Mins;
			   	    	postErrorData.endTime = $scope.endTime;
			   	    
			   	       var Error30mins =  RestApiFactory.getChartData(postErrorData).then(function(data) {
		   	    	 	  
			   	    	 	var errorCount = CommonUtilService.getErrorDataObject(data)
			   	    	 	
			   	    	 	var Error30MinObj = 
			                   		{
			   	    	 			Error30Mins: errorCount
			                   			
			                   		}
			   	    	 	
	                	
			   	    		console.log("Error30MinObj:"+JSON.stringify(Error30MinObj));
	                	
			   	    		$.extend(applicationData, Error30MinObj);
			   	    		
			   	    	 //  $scope.applications.push(applicationData);
		   	    	     //  console.log("$scope.applications=="+JSON.stringify($scope.applications));
	    		 
			   	     });
			   	       
			   	    Error30mins.then(function(){
			   	   //Last 1 hours
			   	     	$scope.startTime = DateService.startDate(1)
			      	    console.log( "startTime========="+$scope.startTime);
			   	     	
			   	     	postErrorData.startTime = $scope.startTime;
			   	    	postErrorData.endTime = $scope.endTime;
			   	     	
			   	     	var Error1Hr =  RestApiFactory.getChartData(postErrorData).then(function(data) {
		   	    	 	  
			   	    	 	var errorCount = CommonUtilService.getErrorDataObject(data)
			   	    	 	
			   	    	 	var Error1HrObj = 
			                   		{
			   	    	 			Error1Hr: errorCount
			                   			
			                   		}
			   	    	 	
	                	
			   	    		console.log("Error1HrObj:"+JSON.stringify(Error1HrObj));
	                	
			   	    		$.extend(applicationData, Error1HrObj);
			   	    		
			   	    	//   $scope.applications.push(applicationData);
		   	    	    //   console.log("$scope.applications=="+JSON.stringify($scope.applications));
	    		 
			   	     });
			   	     	
			   	     Error1Hr.then(function(){
			   	    	 //Last 6 hours
				   	      $scope.startTime = DateService.startDate(6)
				      	  console.log( "startTime========="+$scope.startTime);	 
				   	      
				   	   postErrorData.startTime = $scope.startTime;
			   	    	postErrorData.endTime = $scope.endTime;
				   	      
				   	     var Error6Hr =  RestApiFactory.getChartData(postErrorData).then(function(data) {
			   	    	 	  
			   	    	 	var errorCount = CommonUtilService.getErrorDataObject(data)
			   	    	 	
			   	    	 	var Error6HrObj = 
			                   		{
			   	    	 			Error6Hr: errorCount
			                   			
			                   		}
			   	    	 	
	                	
			   	    		console.log("Error6HrObj:"+JSON.stringify(Error6HrObj));
	                	
			   	    		$.extend(applicationData, Error6HrObj);
			   	    		
			   	    	 //  $scope.applications.push(applicationData);
		   	    	     //  console.log("$scope.applications=="+JSON.stringify($scope.applications));
			   	    	
			   	    	 
			   	     });
				   	     
				   	  Error6Hr.then(function(){
					   		//Above 6 hours
					   	    	
					   	    	$scope.startTime = DateService.startDate(48);
					      	    console.log( "startTime========="+$scope.startTime);
					      	    
					      	    postErrorData.startTime = $scope.startTime;
					   	    	postErrorData.endTime = $scope.endTime;
					   	    	
					      	    
					      	  var Above6Hr =  RestApiFactory.getChartData(postErrorData).then(function(data) {
				   	    	 	  
					   	    	 	var errorCount = CommonUtilService.getErrorDataObject(data)
					   	    	 	
					   	    	 	var ErrorAbove6HrObj = 
					                   		{
					   	    	 			ErrorAbove6Hr: errorCount
					                   			
					                   		}
					   	    	 	
			                	
					   	    		console.log("ErrorAbove6HrObj:"+JSON.stringify(ErrorAbove6HrObj));
			                	
					   	    		$.extend(applicationData, ErrorAbove6HrObj);
					   	    		
					   	    	   $scope.applications.push(applicationData);
				   	    	       console.log("$scope.applications=="+JSON.stringify($scope.applications));
					   	    	
					   	    	 
					   	     });
					      	  
					        
					   	  });   
			   	    	
			   	    	
			   	    });
			   	     
			   	     
			   	 
			   	       
			   	   		
		   	   	});
			   	    
		   	   	});
		   	    		 
		     	 });
   	    });
    	});
       	});
	       
	      	    
	    	 $scope.dataLoaded1Hr = true; 
		  	 $scope.dataLoaded24Hr = true; 
		  	 $scope.dataLoaded = true;
   	        
	     	});
	     	
	     	console.log("appsList=="+appsList);
	     	
	     	 localStorage.setItem('appsList', appsList);
	     	
	     }
	     	
	     	
	      
	    });
	     
	      
		
	 $scope.date = new Date();
	 
	//Epoch To Date
	 function EpochToDate(epoch) {
	  	 
		 var date = new Date(epoch);
		 
	     return date;
	 }
	 

	 //Chart initigration for guage Avaibility
	 
	 //Indraneel Logic for Availability doughnut color css etc is here
	$scope.availabilityChartConfig1Hr = {
		options: {
			chart: {
				type: 'solidgauge',
				margin: [0, 0, 0, 0]
			},
			credits: {enabled: false},
			title: '',
			tooltip: {enabled: false},
			pane: {
				startAngle: 0,
				endAngle: 360,
				background: [{
					outerRadius: '100%',
					innerRadius: '88%',
					backgroundColor: '#CCC',
					borderWidth: 0
				}]
			},
			plotOptions: {
				solidgauge: {
					dataLabels: {
						y: -1,
						color:'red',
						enabled: true,
						borderWidth: 0,
						verticalAlign: 'middle',
						formatter: function () {
							var clr = '#ccc';
							if(this.y >0 && this.y <=40){
								clr = '#bc0c0c';
							}else if(this.y >40 && this.y <=60){
								clr = '#f1a80c';
							}else if(this.y >60 && this.y <=100){
								clr = '#89c657';
							}
							return '<span style="font-family: Helvetica, Arial, sans-serif; font-weight:100;color: '+ clr +' ;">' + Math.round(this.y) +  '%</span>';
						},
						//format: '<div style="text-align:center">{y}%</div>',
						style: {
					 	"fontSize": "34px"
						},
						useHTML:true

					}
				},
				series: {
					radius: '100%',
					innerRadius: '88%',
				}
			},
		},

		series: [{
			data: [0]
		}],

		yAxis: {
			min: 0,
			max: 100,
			lineWidth: 0,
			tickPositions: [],
			stops: [
				[0, '#bc0c0c'], // red till 40%
				[0.40, '#bc0c0c'], // red till 40%
				[0.41, '#f1a80c'], // red till 40%
				[0.60, '#f1a80c'], // orange till 60%
				[0.61, '#89c657'], // orange till 60%
				[1, '#89c657'] // green above 90%
			],
		},
		func: function (chart) {
			$scope.$evalAsync(function () {
				chart.reflow();
				//The below is an event that will trigger all instances of charts to reflow
				//$scope.$broadcast('highchartsng.reflow');
			});
		},
		loading: false
	};
	$scope.availabilityChartConfig1Hr.series[0].data[0] = 0;
	
	//chart 24 hours for Circle chart
	
	//Indraneel Logic for Availability doughnut color css etc is here
	
	$scope.availabilityChartConfig24Hr = {
			options: {
				chart: {
					type: 'solidgauge',
					margin: [0, 0, 0, 0]
				},
				credits: {enabled: false},
				title: '',
				tooltip: {enabled: false},
				pane: {
					startAngle: 0,
					endAngle: 360,
					background: [{
						outerRadius: '100%',
						innerRadius: '88%',
						backgroundColor: '#CCC',
						borderWidth: 0
					}]
				},
				plotOptions: {
					solidgauge: {
						dataLabels: {
							y: -1,
							color:'red',
							enabled: true,
							borderWidth: 0,
							verticalAlign: 'middle',
							formatter: function () {
								var clr = '#ccc';
								if(this.y >0 && this.y <=40){
									clr = '#bc0c0c';
								}else if(this.y >40 && this.y <=60){
									clr = '#f1a80c';
								}else if(this.y >60 && this.y <=100){
									clr = '#89c657';
								}
								return '<span style="font-family: Helvetica, Arial, sans-serif; font-weight:100;color: '+ clr +' ;">' + Math.round(this.y) +  '%</span>';
							},
							//format: '<div style="text-align:center">{y}%</div>',
							style: {
						 	"fontSize": "34px"
							},
							useHTML:true

						}
					},
					series: {
						radius: '100%',
						innerRadius: '88%',
					}
				},
			},

			series: [{
				data: [0]
			}],

			yAxis: {
				min: 0,
				max: 100,
				lineWidth: 0,
				tickPositions: [],
				stops: [
					[0, '#bc0c0c'], // red till 40%
					[0.40, '#bc0c0c'], // red till 40%
					[0.41, '#f1a80c'], // red till 40%
					[0.60, '#f1a80c'], // orange till 60%
					[0.61, '#89c657'], // orange till 60%
					[1, '#89c657'] // green above 90%
				],
			},
			func: function (chart) {
				$scope.$evalAsync(function () {
					chart.reflow();
					//The below is an event that will trigger all instances of charts to reflow
					//$scope.$broadcast('highchartsng.reflow');
				});
			},
			loading: false
		};
	
	$scope.availabilityChartConfig24Hr.series[0].data[0] = 0;
	
	 Highcharts.setOptions({
	        global: {
	        	useUTC : false
	        }
	    });
	 
	
	
	


	


}]);