'use strict';

/* Display logged in user name in header section  */ 


app.controller('headerCtrl',  function ( $scope, $rootScope) {
	  console.log("headerCtrl reporting for duty.");
	  
	  var userName = $rootScope.userName;
	  if(userName == undefined){
		  userName = localStorage.getItem("userName");
	         console.log("userName=="+userName);
	        $rootScope.userName = userName;
	  }
	  
	  var emailId = $rootScope.emailId;
	  
	  if(emailId == undefined){
		  emailId = localStorage.getItem('emailId');
	         console.log("emailId=="+emailId);
	        $rootScope.emailId = emailId;
	  }
	  
	  
	  var clientName = $rootScope.clientName;
	  
	  if(clientName == undefined){
		  clientName = localStorage.getItem('clientName');
	         console.log("clientName=="+clientName);
	        $rootScope.clientName = clientName;
	  }

		$scope.closePopoverTemplate = function () {
			$scope.isOpen = false;
		};
		
		$rootScope.alertCount = 0;	
	 
	});