'use strict';
//Indraneel epoc time returning service.
app.service('DateService', function(DateUtilService){
    this.startDate = function(a) {
       return DateUtilService.startDate(a);
    }
    
    this.endDate = function() {
        return DateUtilService.endDate();
     }
    
    
    this.epochToJsDate = function (ts){
        // ts = epoch timestamp
        // returns date obj
    	
    	var d = new Date(ts);
    	console.log(d.getHours());
    	console.log(d.getMonth());
    	console.log(d.getMinutes());
    	console.log(d.getDate());
    	console.log(d.getFullYear());
    	
    	var dd = d.getDate();
    	var mm = d.getMonth()+1;
    	var yy = d.getFullYear();
    	var hh = d.getHours();
    	var ss = d.getMinutes();
    	
    	var monthNames = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
    	
    	var month_name = monthNames[mm-1];
    	console.log("month_name==="+month_name);
    	
    	 if(dd<10){
    	        dd='0'+dd
    	    } 
    	    if(mm<10){
    	        mm='0'+mm
    	    } 
    	    
    	  var date1 = dd+"-"+month_name+"-"+yy+" "+hh + ":"+mm;
    	  console.log("date1==="+date1);
    	 
        return date1;
   }
    
    
  //Indraneel Epoch To Date
    this.EpochToDate =   function(epoch) {
	  	 
		 var date = new Date(epoch);
		 
	     return date;
	 }
	 
    
 });


app.service('CommonUtilService', function(){
	
	/*
	 * Setting applicationName and applocmapid for the selected application
	 */ 
	this.setAppData = function(data,applicationName){
		
		var keepGoing = true;
		
		 var appMetaDataArray = [];
		 
		 appMetaDataArray = JSON.parse(localStorage.getItem('appMetaData'));
		 console.log("appMetaData==="+ appMetaDataArray.length);	
  
		angular.forEach(appMetaDataArray, function(item){
			
			 
			if(keepGoing) {
			 var appObject = item;
			 console.log("appObject==="+ JSON.stringify(appObject));
			
			 angular.forEach(appObject, function(item2){
				// console.log("applicationName==="+ appObject.applicationName);
				 var appName = appObject.applicationName;
				 if(keepGoing){
				 if(appName == applicationName){
					// console.log("applicationName found ");
					 var appLocMapId = appObject.appLocMapId;
					// console.log("appLocMapId ==== "+appLocMapId);
					 localStorage.setItem('appLocMapId', appLocMapId);
					 localStorage.setItem('applicationName', applicationName);
					 
					 keepGoing=false;
					
					
				 }
				 }
				 
				
			 });
			}
			 
			
			 
		});
	
	}
	
	this.getErrorDataObject = function(data){
		
		var errorCount = 0;
		
		if(data.statusCode == 1)
 	   	{
	 	 console.log("statusCode=== 1" )
	 	
 	 
 	   	} else{
 		   console.log("statusCode=== 0" ) 
 		   console.log( JSON.stringify(data.response));
 		   errorCount = data.response.state;
 		   console.log("errorCount=== "+errorCount ) ;
 	   }
		
		return errorCount;
		
	}
	
	
	
	this.getPageNames= function(data,transName){
		
				var pages = [];
				console.log("transName=="+ JSON.stringify(transName));
				var keepGoing = true;
		  	
	  			angular.forEach(data, function(transactionItem){
	  				
	  				if(keepGoing) {
	  				var tranItem = transactionItem;	
	  			    console.log("tranItem=="+ JSON.stringify(tranItem));
	  			    
	  			    var transactionName = tranItem.transactionName;
	  			    console.log("transactionName=== "+transactionName );
	  			   
	  			    
	  			     if( transactionName == transName ) {
	  			    	 
	  			    	var transactionChildren = []; 
	     	  			transactionChildren =  tranItem.pages;
	     	  		   console.log("transactionChildren=="+ JSON.stringify(transactionChildren));
	     	  		 
	     	  			angular.forEach(transactionChildren, function(pageItem){
	     	  				
	     	  				var page = pageItem;
	     	  				console.log("page=="+ JSON.stringify(page.pageName));
	     	  				pages.push(page.pageName);
	     	  				
	     	  				
	     	  				
	     	  			});
	  			    	 
	     	  			keepGoing = false;
	  			     }
	  				}
	  			});
		
	  			console.log("pages=="+ JSON.stringify(pages));
	  			return pages;
	}
	
	//Indraneel Setting Tree for Txns in Drill Down.
	this.setTransactionData = function(data){
		
		 var applicationArray = [];
 		 
  		applicationArray = angular.fromJson(data.response.applications);
  		 console.log("applicationArray=== "+ applicationArray );
  		 
  		var treeArray = [];
  		//treeArray.push(allTransactionData);
  		 
  		angular.forEach(applicationArray, function(applicationItem){
  			
  			var appItem = applicationItem;
  			console.log("appLocMapId=== "+appItem.appLocMapId );
  			console.log("applicationId=== "+appItem.applicationId );
  			console.log("applicationName=== "+appItem.applicationName );
  			
  			var transArray = [];
  			var transactionData = {};
  			var transactionArray = [];
  			
  			
  			transArray = angular.fromJson(appItem.transactions);
  			console.log("transArray============ "+transArray);
  			localStorage.setItem('transArray',JSON.stringify(transArray));
  			
  			var allTranChildArray = [];
  			
  			angular.forEach(transArray, function(transctionItem){
  				
  			var tranItem = transctionItem;	
  			console.log("transactionId=== "+tranItem.transactionId );	
  			console.log("transactionName=== "+tranItem.transactionName );
  			//Indraneel Setting each txn node with id , name , checked status
  			
  			 var transactionData = {
  					"id": "transcation_"+tranItem.transactionId,
				 	    "name":tranItem.transactionName,
				 	    "checked": true
			 		    	 	  }
  			
  			
  			
  			var pageArray = [];
  			
  			pageArray = angular.fromJson(tranItem.pages);
  			
  			if(pageArray.length > 0) {
  				
  				var pageChildArray = [];
  				
  				
  			angular.forEach(pageArray, function(pageItem){
  			
  				var page = pageItem;
  				console.log("pageId=== "+page.pageId );
  				console.log("pageName=== "+page.pageName );
  				
  				var pageIdName = {
  						"id": "page_"+ page.pageId,		
  						"name":page.pageName,
  						 "checked": false
  				}
  				
  				pageChildArray.push(pageIdName);
  				
  				
  				
  				
  			});
  			
  			 				    			
  			
  			}
  			
  			var pageData = {
  					"children":pageChildArray			
  			}
  			
  			$.extend(transactionData,pageData);
  			
  			// Indraneel adding all individual txn nodes to master transactionArray . This will be appended in each loop.
  			transactionArray.push(transactionData);
  			
  			
  				
  			}); // end of loop for setting all txn nodes . 
  			
  			//Indraneel Add alll trxn node data under All Transaction
  			var allTransactionData = {
	    				 "name": "All Transactions",
	    				 "id" : "all",
                      "checked": true,
                      "children":transactionArray
	    		}
 			
 			
 			treeArray.push(allTransactionData);
  			
  			localStorage.setItem('treeArray', JSON.stringify(treeArray));
  			
  			
	    	//console.log("$scope.tc.tree=== "+JSON.stringify(treeArray));
  			
		
	});
	}
});


app.service('TimeSeriesService', function(RestApiFactory,DateService,$filter,$q){
	
	
	this.timeSeries = function(data,type){
		
		 console.log("type=="+type);
		 
		 var category = [];
		// var unitChart = data.response.unit;
		// console.log("unitChart======="+unitChart);
		 var Units = "";
		 var multiTimeSeries = [];
		 
		 multiTimeSeries = angular.fromJson(data.response.multiTimeSeries);
	     console.log("multiTimeSeries=="+JSON.stringify(multiTimeSeries)); 
	    
	     var dataset = [];
	     var series = [];
	    
	     for (var key in multiTimeSeries) {
		      var value = multiTimeSeries[key];
		      console.log(key + ", " + JSON.stringify(value));
		      var len = category.length;
		      console.log(" len======="+ len);
			  console.log(" key======="+ key);
			  var seriesName = key;
			  console.log(" seriesName======="+ seriesName);
			  var data = [];
		  
		      angular.forEach(value, function(item){
		    	  console.log("item======="+JSON.stringify(item));
		    	  
		    	  var dataset = [];
		    	 // var dataValue = item.value / 1000;
		    	  
		    	  var dataValue = 0;
		    	  
		    	  if(type == "availability"){
		    		  dataValue = item.value 
		    		  console.log("dataValue======="+dataValue);
		    		  
		    	  }else{
		    		  dataValue = item.value / 1000  ;  
		    		  
		    	  }
		    	  
		    	  var time = item.time;
	 			  console.log("time======="+time);
	 			
		    	  dataset.push(time);
		    	  dataset.push(dataValue);
		    	 
	    		  
	    		
			      
			      console.log("dataset=="+JSON.stringify(dataset)); 
			    	
			    	data.push(dataset);
			    	
   		    	
		 		 }); 
	 		 
		 //   console.log("category=="+JSON.stringify(category));
		    console.log("data=="+JSON.stringify(data));
		   
		    var seriesDataset = {
				  "name": seriesName,
				  "data": data
		    }
		   
		    series.push(seriesDataset);
		   
		
	    
	     }
	    console.log("series=="+JSON.stringify(series));
		  
		
		 var timeSeriesData  = series;
		 console.log("timeSeriesData=="+ JSON.stringify(timeSeriesData));
		 
		 return timeSeriesData;
   
	
	
}
	
	this.BarData = function(data,type){
		 console.log("BarData method is invoked");	
		 console.log("type=="+type);
		 
		 var availabilityBarDetails = [];
	 	 availabilityBarDetails = angular.fromJson(data.response.aggregateBarChart);
		 console.log("availabilityBarDetails=="+availabilityBarDetails);
		 
		 var aggregateBarData  = [];
 		 angular.forEach(availabilityBarDetails, function(availabilityBarItem){
 			
 			var value = availabilityBarItem.value;
 			var lable = availabilityBarItem.label;
 			var color = {};
 			if(value >= 70){
 				
 				color = {
					linearGradient: { x1: 0, x2: 1, y1: 0, y2: 0 },
					stops: [
						[0, '#4b9012'],
						[1, '#88c555']
					]
				}
 				
 			} else if (value >= 50){
 				
 				//color = "#F98020";
 				
 				color =	{
					linearGradient: { x1: 0, x2: 1, y1: 0, y2: 0 },
					stops: [
						[0, '#4b9012'],
						[1, '#88c555']
					]
				}
 				
 			} else if (value >= 40) {
 				
 				//color = "#DB4939";
 				color =	{
					linearGradient: { x1: 0, x2: 1, y1: 0, y2: 0 },
					stops: [
						[0, '#c76719'],
						[1, '#f67f1f']
					]
				}
 			} else  {
 				
 				//color = "#DB4939";
 				color =	{
 						linearGradient: { x1: 0, x2: 1, y1: 0, y2: 0 },
 						stops: [
 							[0, '#a81f10'],
 							[1, '#d94839']
 						]
				}
 			}
 			
 			var barDataObj = {
	    				   "name" : lable,
	    				   "y" : value,
	    				   "color" : color
	    			  }
	    		   
 			aggregateBarData.push(barDataObj);
 			
 		});
 		
 		console.log("aggregateBarData=="+ JSON.stringify(aggregateBarData));  
 		
 		var seriesBarObj = {
 				    name: type ,
			        colorByPoint: true,
			        data : aggregateBarData
 		}
 		
 		return seriesBarObj;

	}
	
	
	/*this.timeSeries = function(data){
			 var category = [];
			 var unitChart = data.response.unit;
			 console.log("unitChart======="+unitChart);
			 var Units = "";
			 var multiTimeSeries = [];
			 
			 multiTimeSeries = angular.fromJson(data.response.multiTimeSeries);
		     console.log("multiTimeSeries=="+JSON.stringify(multiTimeSeries)); 
		    
		     var dataset = [];
		    
		     for (var key in multiTimeSeries) {
			      var value = multiTimeSeries[key];
			      console.log(key + ", " + JSON.stringify(value));
			      var len = category.length;
			      console.log(" len======="+ len);
				  console.log(" key======="+ key);
				  var seriesName = key;
				  console.log(" seriesName======="+ seriesName);
				  var data = [];
			  
			      angular.forEach(value, function(item){
			    	  console.log("item======="+item);
			    	  var dataValue = item.value;
			    	  var time = item.time;
		 			
			    	  console.log("dataValue======="+dataValue);
			    	  console.log("time======="+time);
		 			
			    	  var date1 = DateService.EpochToDate(time);
			    	  console.log("date1=="+date1);
 		    		 
			    	  if(len == 0) {
			    	  if(unitChart == "Hourly"){
			    		 Units = $filter('date')(new Date(date1),'hh');
			    		  console.log("Hours:"+ Units);
			    	  }else if (unitChart == "Daily"){
			    		  Units = $filter('date')(new Date(date1),'dd');
			    		  console.log("Daily:"+ Units);
			    	  }else if (unitChart == "Monthly"){
     		    		  Units = $filter('date')(new Date(date1),'MMM');
		      		       console.log("Monthly:"+ Units);
     		    	 }
     		    	 
	      		   	var value = Units;
 		    	 	var categaoryData = {
 		    	 	    "label": value
 		    	 	  }
		    		 
 		    	 	category.push(categaoryData);
 		    	 	
 		    		}
 		    		
			    	var dataDataset = {
     		    	    "value":  dataValue
     		    	}
     		    		 
     		    	data.push(dataDataset);
     		    	
		 		 }); 
		 		 
			    console.log("category=="+JSON.stringify(category));
			    console.log("data=="+JSON.stringify(data));
			   
			    var seriesDataset = {
					  "seriesname": seriesName,
					  "data": data
			    }
			   
			    dataset.push(seriesDataset);
			   
			}
		    
		    console.log("dataset=="+JSON.stringify(dataset));
			var timeSeriesData = {};
   		
   		  	var timeseriesAttributeData = {
			 
			 "chart": {
			        
			        "captionFontSize": "14",
			        "subcaptionFontSize": "14",
			        "subcaptionFontBold": "0",
			        "paletteColors": "#FB24EB,#20FFFF,#0075c2,#1aaf5d",
			        "bgcolor": "#ffffff",
			        "showBorder": "0",
			        "showShadow": "0",
			        "showCanvasBorder": "0",
			        "usePlotGradientColor": "0",
			        "legendBorderAlpha": "0",
			        "legendShadow": "0",
			        "showAxisLines": "0",
			        "showAlternateHGridColor": "0",
			        "divlineThickness": "1",
			        "divLineIsDashed": "1",
			        "divLineDashLen": "1",
			        "divLineGapLen": "1",
			        "xAxisName": unitChart,
			        "showValues": "0",
			        "showLegend": "1",
	                "legendPosition": "RIGHT"
			    }
   		  };
   		  
   		  $.extend(timeSeriesData,timeseriesAttributeData);
   		  console.log("timeSeriesData=="+ JSON.stringify(timeSeriesData));
   		  
   		  var categories = [];
   		  
   		  var categoryObj = {
   				  "category": category   
   		  }
   		  
   		  categories.push(categoryObj);
   		  
   		  var timeSeriresdataSet = {
   				  "categories": categories,
   				  "dataset": dataset
   				  
   		  }
   		  
   		  $.extend(timeSeriesData,timeSeriresdataSet);
   		  console.log("timeSeriesData=="+ JSON.stringify(timeSeriesData));
   		  
   		 var myPerfDataSource = timeSeriesData;
   		 console.log("myPerfDataSource=="+ JSON.stringify(myPerfDataSource));
   		 
   		 return myPerfDataSource;
	    
		
		
	}*/
	
	this.getTransactionName = function(){
		
		var transArray = [];
		var transNames = [];
		transArray = JSON.parse(localStorage.getItem('transArray'));
		var length = transArray.length;
		console.log("length=== "+length );	
		
		if(length == 0){
			console.log("length is zero=== ");	
		}
		
		angular.forEach(transArray, function(transctionItem){
			 var tranItem = transctionItem;	
			 console.log("transactionId=== "+tranItem.transactionId );	
			 console.log("transactionName=== "+tranItem.transactionName );
			 var transactionName = tranItem.transactionName;
			 var transElement =  transactionName;
			 transNames.push(transElement);
		  });
		console.log("transNames=== "+transNames );	
		return transNames;
		
	}
	
	
});







